// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouterGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:auto_route/auto_route.dart' as _i7;
import 'package:flutter/material.dart' as _i8;
import 'package:recipes_app_1/models/recipes/recipes_model.dart' as _i9;
import 'package:recipes_app_1/models/restaurant/restaurant_model.dart' as _i10;
import 'package:recipes_app_1/screens/home_screen.dart' as _i1;
import 'package:recipes_app_1/screens/orders_screen.dart' as _i2;
import 'package:recipes_app_1/screens/recipe_info_screen.dart' as _i3;
import 'package:recipes_app_1/screens/recipes_screen.dart' as _i4;
import 'package:recipes_app_1/screens/restaurants_screen.dart' as _i6;
import 'package:recipes_app_1/screens/resturant_info_screen.dart' as _i5;

abstract class $AppRouter extends _i7.RootStackRouter {
  $AppRouter({super.navigatorKey});

  @override
  final Map<String, _i7.PageFactory> pagesMap = {
    HomeRoute.name: (routeData) {
      return _i7.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i1.HomeScreen(),
      );
    },
    OrdersRoute.name: (routeData) {
      return _i7.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i2.OrdersScreen(),
      );
    },
    RecipesInfoRoute.name: (routeData) {
      final args = routeData.argsAs<RecipesInfoRouteArgs>();
      return _i7.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: _i3.RecipesInfoScreen(
          key: args.key,
          recipe: args.recipe,
          restaurant: args.restaurant,
        ),
      );
    },
    RecipesRoute.name: (routeData) {
      return _i7.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i4.RecipesScreen(),
      );
    },
    RestaurantInfoRoute.name: (routeData) {
      final args = routeData.argsAs<RestaurantInfoRouteArgs>();
      return _i7.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: _i5.RestaurantInfoScreen(
          key: args.key,
          restaurant: args.restaurant,
          restaurantRecipes: args.restaurantRecipes,
        ),
      );
    },
    RestaurantsRoute.name: (routeData) {
      return _i7.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i6.RestaurantsScreen(),
      );
    },
  };
}

/// generated route for
/// [_i1.HomeScreen]
class HomeRoute extends _i7.PageRouteInfo<void> {
  const HomeRoute({List<_i7.PageRouteInfo>? children})
      : super(
          HomeRoute.name,
          initialChildren: children,
        );

  static const String name = 'HomeRoute';

  static const _i7.PageInfo<void> page = _i7.PageInfo<void>(name);
}

/// generated route for
/// [_i2.OrdersScreen]
class OrdersRoute extends _i7.PageRouteInfo<void> {
  const OrdersRoute({List<_i7.PageRouteInfo>? children})
      : super(
          OrdersRoute.name,
          initialChildren: children,
        );

  static const String name = 'OrdersRoute';

  static const _i7.PageInfo<void> page = _i7.PageInfo<void>(name);
}

/// generated route for
/// [_i3.RecipesInfoScreen]
class RecipesInfoRoute extends _i7.PageRouteInfo<RecipesInfoRouteArgs> {
  RecipesInfoRoute({
    _i8.Key? key,
    required _i9.RecipeModel recipe,
    required _i10.RestaurantModel restaurant,
    List<_i7.PageRouteInfo>? children,
  }) : super(
          RecipesInfoRoute.name,
          args: RecipesInfoRouteArgs(
            key: key,
            recipe: recipe,
            restaurant: restaurant,
          ),
          initialChildren: children,
        );

  static const String name = 'RecipesInfoRoute';

  static const _i7.PageInfo<RecipesInfoRouteArgs> page =
      _i7.PageInfo<RecipesInfoRouteArgs>(name);
}

class RecipesInfoRouteArgs {
  const RecipesInfoRouteArgs({
    this.key,
    required this.recipe,
    required this.restaurant,
  });

  final _i8.Key? key;

  final _i9.RecipeModel recipe;

  final _i10.RestaurantModel restaurant;

  @override
  String toString() {
    return 'RecipesInfoRouteArgs{key: $key, recipe: $recipe, restaurant: $restaurant}';
  }
}

/// generated route for
/// [_i4.RecipesScreen]
class RecipesRoute extends _i7.PageRouteInfo<void> {
  const RecipesRoute({List<_i7.PageRouteInfo>? children})
      : super(
          RecipesRoute.name,
          initialChildren: children,
        );

  static const String name = 'RecipesRoute';

  static const _i7.PageInfo<void> page = _i7.PageInfo<void>(name);
}

/// generated route for
/// [_i5.RestaurantInfoScreen]
class RestaurantInfoRoute extends _i7.PageRouteInfo<RestaurantInfoRouteArgs> {
  RestaurantInfoRoute({
    _i8.Key? key,
    required _i10.RestaurantModel restaurant,
    required List<_i9.RecipeModel> restaurantRecipes,
    List<_i7.PageRouteInfo>? children,
  }) : super(
          RestaurantInfoRoute.name,
          args: RestaurantInfoRouteArgs(
            key: key,
            restaurant: restaurant,
            restaurantRecipes: restaurantRecipes,
          ),
          initialChildren: children,
        );

  static const String name = 'RestaurantInfoRoute';

  static const _i7.PageInfo<RestaurantInfoRouteArgs> page =
      _i7.PageInfo<RestaurantInfoRouteArgs>(name);
}

class RestaurantInfoRouteArgs {
  const RestaurantInfoRouteArgs({
    this.key,
    required this.restaurant,
    required this.restaurantRecipes,
  });

  final _i8.Key? key;

  final _i10.RestaurantModel restaurant;

  final List<_i9.RecipeModel> restaurantRecipes;

  @override
  String toString() {
    return 'RestaurantInfoRouteArgs{key: $key, restaurant: $restaurant, restaurantRecipes: $restaurantRecipes}';
  }
}

/// generated route for
/// [_i6.RestaurantsScreen]
class RestaurantsRoute extends _i7.PageRouteInfo<void> {
  const RestaurantsRoute({List<_i7.PageRouteInfo>? children})
      : super(
          RestaurantsRoute.name,
          initialChildren: children,
        );

  static const String name = 'RestaurantsRoute';

  static const _i7.PageInfo<void> page = _i7.PageInfo<void>(name);
}
