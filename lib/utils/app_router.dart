import 'package:auto_route/auto_route.dart';
import 'app_router.gr.dart';

@AutoRouterConfig(replaceInRouteName: 'Screen,Route')
class AppRouter extends $AppRouter {
  @override
  List<AutoRoute> get routes => [
        AutoRoute(
            path: '/home',
            page: HomeRoute.page,
            initial: true,
            children: [
              AutoRoute(
                path: 'recipes',
                page: RecipesRoute.page,
              ),
              AutoRoute(
                path: 'orders',
                page: OrdersRoute.page,
              ),
              AutoRoute(
                path: 'restaurants',
                page: RestaurantsRoute.page,
              ),
            ]),
        AutoRoute(
          path: '/restaurant_info',
          page: RestaurantInfoRoute.page,
        ),
        AutoRoute(
          path: '/recipes_info',
          page: RecipesInfoRoute.page,
        ),
      ];
}
