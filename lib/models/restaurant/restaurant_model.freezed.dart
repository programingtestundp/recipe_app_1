// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'restaurant_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$RestaurantModel {
  String get name => throw _privateConstructorUsedError;
  String get objectId => throw _privateConstructorUsedError;
  String get location => throw _privateConstructorUsedError;
  String get image_link => throw _privateConstructorUsedError;
  List<String> get orders_ids => throw _privateConstructorUsedError;
  int get stars => throw _privateConstructorUsedError;
  bool get accept_external_order => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $RestaurantModelCopyWith<RestaurantModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $RestaurantModelCopyWith<$Res> {
  factory $RestaurantModelCopyWith(
          RestaurantModel value, $Res Function(RestaurantModel) then) =
      _$RestaurantModelCopyWithImpl<$Res, RestaurantModel>;
  @useResult
  $Res call(
      {String name,
      String objectId,
      String location,
      String image_link,
      List<String> orders_ids,
      int stars,
      bool accept_external_order});
}

/// @nodoc
class _$RestaurantModelCopyWithImpl<$Res, $Val extends RestaurantModel>
    implements $RestaurantModelCopyWith<$Res> {
  _$RestaurantModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? name = null,
    Object? objectId = null,
    Object? location = null,
    Object? image_link = null,
    Object? orders_ids = null,
    Object? stars = null,
    Object? accept_external_order = null,
  }) {
    return _then(_value.copyWith(
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      objectId: null == objectId
          ? _value.objectId
          : objectId // ignore: cast_nullable_to_non_nullable
              as String,
      location: null == location
          ? _value.location
          : location // ignore: cast_nullable_to_non_nullable
              as String,
      image_link: null == image_link
          ? _value.image_link
          : image_link // ignore: cast_nullable_to_non_nullable
              as String,
      orders_ids: null == orders_ids
          ? _value.orders_ids
          : orders_ids // ignore: cast_nullable_to_non_nullable
              as List<String>,
      stars: null == stars
          ? _value.stars
          : stars // ignore: cast_nullable_to_non_nullable
              as int,
      accept_external_order: null == accept_external_order
          ? _value.accept_external_order
          : accept_external_order // ignore: cast_nullable_to_non_nullable
              as bool,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_RestaurantModelCopyWith<$Res>
    implements $RestaurantModelCopyWith<$Res> {
  factory _$$_RestaurantModelCopyWith(
          _$_RestaurantModel value, $Res Function(_$_RestaurantModel) then) =
      __$$_RestaurantModelCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String name,
      String objectId,
      String location,
      String image_link,
      List<String> orders_ids,
      int stars,
      bool accept_external_order});
}

/// @nodoc
class __$$_RestaurantModelCopyWithImpl<$Res>
    extends _$RestaurantModelCopyWithImpl<$Res, _$_RestaurantModel>
    implements _$$_RestaurantModelCopyWith<$Res> {
  __$$_RestaurantModelCopyWithImpl(
      _$_RestaurantModel _value, $Res Function(_$_RestaurantModel) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? name = null,
    Object? objectId = null,
    Object? location = null,
    Object? image_link = null,
    Object? orders_ids = null,
    Object? stars = null,
    Object? accept_external_order = null,
  }) {
    return _then(_$_RestaurantModel(
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      objectId: null == objectId
          ? _value.objectId
          : objectId // ignore: cast_nullable_to_non_nullable
              as String,
      location: null == location
          ? _value.location
          : location // ignore: cast_nullable_to_non_nullable
              as String,
      image_link: null == image_link
          ? _value.image_link
          : image_link // ignore: cast_nullable_to_non_nullable
              as String,
      orders_ids: null == orders_ids
          ? _value._orders_ids
          : orders_ids // ignore: cast_nullable_to_non_nullable
              as List<String>,
      stars: null == stars
          ? _value.stars
          : stars // ignore: cast_nullable_to_non_nullable
              as int,
      accept_external_order: null == accept_external_order
          ? _value.accept_external_order
          : accept_external_order // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$_RestaurantModel extends _RestaurantModel {
  const _$_RestaurantModel(
      {required this.name,
      required this.objectId,
      required this.location,
      required this.image_link,
      required final List<String> orders_ids,
      required this.stars,
      required this.accept_external_order})
      : _orders_ids = orders_ids,
        super._();

  @override
  final String name;
  @override
  final String objectId;
  @override
  final String location;
  @override
  final String image_link;
  final List<String> _orders_ids;
  @override
  List<String> get orders_ids {
    if (_orders_ids is EqualUnmodifiableListView) return _orders_ids;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_orders_ids);
  }

  @override
  final int stars;
  @override
  final bool accept_external_order;

  @override
  String toString() {
    return 'RestaurantModel(name: $name, objectId: $objectId, location: $location, image_link: $image_link, orders_ids: $orders_ids, stars: $stars, accept_external_order: $accept_external_order)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_RestaurantModel &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.objectId, objectId) ||
                other.objectId == objectId) &&
            (identical(other.location, location) ||
                other.location == location) &&
            (identical(other.image_link, image_link) ||
                other.image_link == image_link) &&
            const DeepCollectionEquality()
                .equals(other._orders_ids, _orders_ids) &&
            (identical(other.stars, stars) || other.stars == stars) &&
            (identical(other.accept_external_order, accept_external_order) ||
                other.accept_external_order == accept_external_order));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      name,
      objectId,
      location,
      image_link,
      const DeepCollectionEquality().hash(_orders_ids),
      stars,
      accept_external_order);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_RestaurantModelCopyWith<_$_RestaurantModel> get copyWith =>
      __$$_RestaurantModelCopyWithImpl<_$_RestaurantModel>(this, _$identity);
}

abstract class _RestaurantModel extends RestaurantModel {
  const factory _RestaurantModel(
      {required final String name,
      required final String objectId,
      required final String location,
      required final String image_link,
      required final List<String> orders_ids,
      required final int stars,
      required final bool accept_external_order}) = _$_RestaurantModel;
  const _RestaurantModel._() : super._();

  @override
  String get name;
  @override
  String get objectId;
  @override
  String get location;
  @override
  String get image_link;
  @override
  List<String> get orders_ids;
  @override
  int get stars;
  @override
  bool get accept_external_order;
  @override
  @JsonKey(ignore: true)
  _$$_RestaurantModelCopyWith<_$_RestaurantModel> get copyWith =>
      throw _privateConstructorUsedError;
}
