import 'package:freezed_annotation/freezed_annotation.dart';
part 'restaurant_model.freezed.dart';
// part 'restaurant_model.g.dart';

@freezed
class RestaurantModel with _$RestaurantModel {
  const RestaurantModel._();
  const factory RestaurantModel({
    required String name,
    required String objectId,
    required String location,
    required String image_link,
    required List<String> orders_ids,
    required int stars,
    required bool accept_external_order,
  }) = _RestaurantModel;

  factory RestaurantModel.fromJson(Map<String, dynamic> json) {
    return RestaurantModel(
      name: json['name'],
      objectId: json['objectId'],
      orders_ids: json['orders_ids'].values.toList().cast<String>(),
      accept_external_order: json['accept_external_order'],
      location: json['location'],
      stars: json['stars'],
      image_link: json['image_link'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'name': name,
      'objectId': objectId,
      'orders_ids': orders_ids.asMap(),
      'accept_external_order': accept_external_order,
      'location': location,
      'stars': stars,
      'image_link': image_link,
    };
  }
}
