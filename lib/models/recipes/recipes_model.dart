import 'package:freezed_annotation/freezed_annotation.dart';
part 'recipes_model.freezed.dart';

@freezed
class RecipeModel with _$RecipeModel {
  const RecipeModel._();
  const factory RecipeModel(
      {required String name,
      String? objectId,
      required String image_link,
      required List<String> ingredients,
      required String description,
      required String category,
      required double price,
      required String restaurant_id}) = _RecipeModel;

  factory RecipeModel.fromJson(Map<String, dynamic> json) {
    return RecipeModel(
        name: json['name'],
        objectId: json['objectId'],
        ingredients: json['ingredients'].values.toList().cast<String>(),
        description: json['description'],
        category: json['category'],
        image_link: json['image_link'],
        price: json['price'],
        restaurant_id: json['restaurant_id']);
  }

  Map<String, dynamic> toJson() {
    return {
      'name': name,
      'ingredients': {
        ...ingredients
            .asMap()
            .map((key, value) => MapEntry(key.toString(), value))
      },
      'description': description,
      'category': category,
      'restaurant_id': restaurant_id,
      'image_link': image_link,
      'price': price,
    };
  }
}
