import 'package:freezed_annotation/freezed_annotation.dart';
part 'order_model.freezed.dart';
part 'order_model.g.dart';

@freezed
class OrderModel with _$OrderModel {
  const OrderModel._();
  const factory OrderModel(
      {required String user_id,
      String? objectId,
      required String restaurant_id,
      required String recipe_id,
      @Default(false) is_privet_order,
      @Default('waiting') String status}) = _OrderModel;

  factory OrderModel.fromJson(Map<String, dynamic> json) =>
      _$OrderModelFromJson(json);
}
