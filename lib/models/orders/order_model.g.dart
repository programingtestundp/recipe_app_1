// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'order_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_OrderModel _$$_OrderModelFromJson(Map<String, dynamic> json) =>
    _$_OrderModel(
      user_id: json['user_id'] as String,
      objectId: json['objectId'] as String?,
      restaurant_id: json['restaurant_id'] as String,
      recipe_id: json['recipe_id'] as String,
      is_privet_order: json['is_privet_order'] ?? false,
      status: json['status'] as String? ?? 'waiting',
    );

Map<String, dynamic> _$$_OrderModelToJson(_$_OrderModel instance) =>
    <String, dynamic>{
      'user_id': instance.user_id,
      'objectId': instance.objectId,
      'restaurant_id': instance.restaurant_id,
      'recipe_id': instance.recipe_id,
      'is_privet_order': instance.is_privet_order,
      'status': instance.status,
    };
