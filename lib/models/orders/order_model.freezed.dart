// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'order_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

OrderModel _$OrderModelFromJson(Map<String, dynamic> json) {
  return _OrderModel.fromJson(json);
}

/// @nodoc
mixin _$OrderModel {
  String get user_id => throw _privateConstructorUsedError;
  String? get objectId => throw _privateConstructorUsedError;
  String get restaurant_id => throw _privateConstructorUsedError;
  String get recipe_id => throw _privateConstructorUsedError;
  dynamic get is_privet_order => throw _privateConstructorUsedError;
  String get status => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $OrderModelCopyWith<OrderModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $OrderModelCopyWith<$Res> {
  factory $OrderModelCopyWith(
          OrderModel value, $Res Function(OrderModel) then) =
      _$OrderModelCopyWithImpl<$Res, OrderModel>;
  @useResult
  $Res call(
      {String user_id,
      String? objectId,
      String restaurant_id,
      String recipe_id,
      dynamic is_privet_order,
      String status});
}

/// @nodoc
class _$OrderModelCopyWithImpl<$Res, $Val extends OrderModel>
    implements $OrderModelCopyWith<$Res> {
  _$OrderModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? user_id = null,
    Object? objectId = freezed,
    Object? restaurant_id = null,
    Object? recipe_id = null,
    Object? is_privet_order = freezed,
    Object? status = null,
  }) {
    return _then(_value.copyWith(
      user_id: null == user_id
          ? _value.user_id
          : user_id // ignore: cast_nullable_to_non_nullable
              as String,
      objectId: freezed == objectId
          ? _value.objectId
          : objectId // ignore: cast_nullable_to_non_nullable
              as String?,
      restaurant_id: null == restaurant_id
          ? _value.restaurant_id
          : restaurant_id // ignore: cast_nullable_to_non_nullable
              as String,
      recipe_id: null == recipe_id
          ? _value.recipe_id
          : recipe_id // ignore: cast_nullable_to_non_nullable
              as String,
      is_privet_order: freezed == is_privet_order
          ? _value.is_privet_order
          : is_privet_order // ignore: cast_nullable_to_non_nullable
              as dynamic,
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_OrderModelCopyWith<$Res>
    implements $OrderModelCopyWith<$Res> {
  factory _$$_OrderModelCopyWith(
          _$_OrderModel value, $Res Function(_$_OrderModel) then) =
      __$$_OrderModelCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String user_id,
      String? objectId,
      String restaurant_id,
      String recipe_id,
      dynamic is_privet_order,
      String status});
}

/// @nodoc
class __$$_OrderModelCopyWithImpl<$Res>
    extends _$OrderModelCopyWithImpl<$Res, _$_OrderModel>
    implements _$$_OrderModelCopyWith<$Res> {
  __$$_OrderModelCopyWithImpl(
      _$_OrderModel _value, $Res Function(_$_OrderModel) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? user_id = null,
    Object? objectId = freezed,
    Object? restaurant_id = null,
    Object? recipe_id = null,
    Object? is_privet_order = freezed,
    Object? status = null,
  }) {
    return _then(_$_OrderModel(
      user_id: null == user_id
          ? _value.user_id
          : user_id // ignore: cast_nullable_to_non_nullable
              as String,
      objectId: freezed == objectId
          ? _value.objectId
          : objectId // ignore: cast_nullable_to_non_nullable
              as String?,
      restaurant_id: null == restaurant_id
          ? _value.restaurant_id
          : restaurant_id // ignore: cast_nullable_to_non_nullable
              as String,
      recipe_id: null == recipe_id
          ? _value.recipe_id
          : recipe_id // ignore: cast_nullable_to_non_nullable
              as String,
      is_privet_order: freezed == is_privet_order
          ? _value.is_privet_order!
          : is_privet_order,
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_OrderModel extends _OrderModel {
  const _$_OrderModel(
      {required this.user_id,
      this.objectId,
      required this.restaurant_id,
      required this.recipe_id,
      this.is_privet_order = false,
      this.status = 'waiting'})
      : super._();

  factory _$_OrderModel.fromJson(Map<String, dynamic> json) =>
      _$$_OrderModelFromJson(json);

  @override
  final String user_id;
  @override
  final String? objectId;
  @override
  final String restaurant_id;
  @override
  final String recipe_id;
  @override
  @JsonKey()
  final dynamic is_privet_order;
  @override
  @JsonKey()
  final String status;

  @override
  String toString() {
    return 'OrderModel(user_id: $user_id, objectId: $objectId, restaurant_id: $restaurant_id, recipe_id: $recipe_id, is_privet_order: $is_privet_order, status: $status)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_OrderModel &&
            (identical(other.user_id, user_id) || other.user_id == user_id) &&
            (identical(other.objectId, objectId) ||
                other.objectId == objectId) &&
            (identical(other.restaurant_id, restaurant_id) ||
                other.restaurant_id == restaurant_id) &&
            (identical(other.recipe_id, recipe_id) ||
                other.recipe_id == recipe_id) &&
            const DeepCollectionEquality()
                .equals(other.is_privet_order, is_privet_order) &&
            (identical(other.status, status) || other.status == status));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, user_id, objectId, restaurant_id,
      recipe_id, const DeepCollectionEquality().hash(is_privet_order), status);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_OrderModelCopyWith<_$_OrderModel> get copyWith =>
      __$$_OrderModelCopyWithImpl<_$_OrderModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_OrderModelToJson(
      this,
    );
  }
}

abstract class _OrderModel extends OrderModel {
  const factory _OrderModel(
      {required final String user_id,
      final String? objectId,
      required final String restaurant_id,
      required final String recipe_id,
      final dynamic is_privet_order,
      final String status}) = _$_OrderModel;
  const _OrderModel._() : super._();

  factory _OrderModel.fromJson(Map<String, dynamic> json) =
      _$_OrderModel.fromJson;

  @override
  String get user_id;
  @override
  String? get objectId;
  @override
  String get restaurant_id;
  @override
  String get recipe_id;
  @override
  dynamic get is_privet_order;
  @override
  String get status;
  @override
  @JsonKey(ignore: true)
  _$$_OrderModelCopyWith<_$_OrderModel> get copyWith =>
      throw _privateConstructorUsedError;
}
