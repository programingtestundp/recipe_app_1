part of 'recipes_bloc.dart';

@freezed
sealed class RecipesEvent with _$RecipesEvent {
  // get the data from the server and update the ui
  const factory RecipesEvent.initial() = RecipesInitialEvent;
  //fil the orders recipes list for OrderScreen
  const factory RecipesEvent.fillOrdersList() = RecipesFillOrdersListEvent;
//order user recipe
  const factory RecipesEvent.orderMyRecipe({required RecipeModel recipe}) =
      RecipesOrderMyRecipeEvent;
  //order recipe from restaurant
  const factory RecipesEvent.orderRecipe({required RecipeModel recipe}) =
      RecipesOrderRecipeEvent;
  //view error message
  const factory RecipesEvent.showError(String error) = RecipesErrorEvent;
  //for delete order
  const factory RecipesEvent.deleteOrder(OrderModel order) =
      RecipesDeleteOrderEvent;
}
