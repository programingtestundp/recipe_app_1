import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:recipes_app_1/data/services/api_services.dart';
import 'package:recipes_app_1/models/orders/order_model.dart';
import 'package:recipes_app_1/models/recipes/recipes_model.dart';
import 'package:recipes_app_1/models/restaurant/restaurant_model.dart';

import '../data/config/exceptions_hundler.dart';
part 'recipes_bloc.freezed.dart';
part 'recipes_bloc.g.dart';

part 'recipes_event.dart';
part 'recipes_state.dart';

class RecipesBloc extends Bloc<RecipesEvent, RecipesState> {
  RecipesBloc() : super(const RecipesState.initialState()) {
    on<RecipesInitialEvent>(_initial);
    on<RecipesOrderMyRecipeEvent>(_onRecipesOrderMyRecipe);
    on<RecipesOrderRecipeEvent>(_onRecipesOrderRecipe);
    on<RecipesErrorEvent>(_onRecipesErrorEvent);
    on<RecipesDeleteOrderEvent>(_onRecipesDeleteOrderEvent);
    on<RecipesFillOrdersListEvent>(_onFillOrdersListEvent);
  }
  final apiServices = APIServices.create();

  //get data at the start
  FutureOr<void> _initial(
      RecipesEvent event, Emitter<RecipesState> emit) async {
    final results = await Future.wait([
      HandleException.handle(apiServices.getRecipes),
      HandleException.handle(apiServices.getRestaurants),
      HandleException.handle(apiServices.getMyRecipes),
      HandleException.handle(apiServices.getMyOrders),
    ]);
    final errors = results.where((result) => result.failure != null).toList();
    if (errors.isNotEmpty) {
      final errorMessage = errors.first.failure!.message;
      debugPrint('Error: $errorMessage');
      final newState = state.maybeMap(
          orElse: () => const RecipesState.mainState(),
          initialState: (state) =>
              RecipesState.mainState(errorMessage: errorMessage));
      emit(newState);
    } else {
      final recipes = results.first.success!.response as List<RecipeModel>;

      final restaurants = results[1].success!.response as List<RestaurantModel>;
      final myRecipes = results[2].success!.response as List<RecipeModel>;
      final myOrders = results[3].success!.response as List<OrderModel>;

      final newState = state.maybeMap(
          orElse: () => const RecipesState.mainState(),
          initialState: (state) => RecipesState.mainState(
              errorMessage: '',
              recipes: recipes,
              myRecipes: myRecipes,
              restaurants: restaurants,
              myOrders: myOrders,
              appLoadingStatus: AppLoadingStatus.done));

      emit(newState);
    }
  }

//create  privet request for recipe from specific restaurant
  FutureOr<void> _onRecipesOrderMyRecipe(
      RecipesOrderMyRecipeEvent event, Emitter<RecipesState> emit) async {
    final recipe = event.recipe;
    //add myRecipe to myRecipes table

    await HandleException.handle(() => apiServices.createMyRecipe(recipe))
        .then((result) async {
      if (result.success != null) {
        final RecipeModel newRecipe = result.success!.response;
        //add order to orders table
        await HandleException.handle(() => apiServices.createOrder(OrderModel(
            user_id: '0',
            is_privet_order: true,
            restaurant_id: recipe.restaurant_id,
            recipe_id: newRecipe.objectId!))).then((result) {
          if (result.success != null) {
            final OrderModel newOrder = result.success!.response;

            //update the state after success
            state.maybeMap(
              orElse: () {},
              mainState: (state) {
                final List<OrderModel> newOrders = List.from(state.myOrders)
                  ..add(newOrder);
                final List<RecipeModel> myRecipesNew =
                    List.from(state.myRecipes)..add(newRecipe);
                final newState = state.copyWith(
                    myOrders: newOrders,
                    myRecipes: myRecipesNew,
                    errorMessage: '');

                emit(newState);
              },
            );
          } else {
            errorFun(errorMessage: result.failure!.message, emit: emit);
          }
        });
      } else {
        errorFun(errorMessage: result.failure!.message, emit: emit);
      }
    });
  }

//create an order for recipe from market
  FutureOr<void> _onRecipesOrderRecipe(
      RecipesOrderRecipeEvent event, Emitter<RecipesState> emit) async {
    final recipe = event.recipe;
    //add order to orders table
    var result = await HandleException.handle(() => apiServices.createOrder(
        OrderModel(
            user_id: '0',
            restaurant_id: recipe.restaurant_id,
            recipe_id: recipe.objectId!)));
    if (result.success != null) {
      final OrderModel newOrder = result.success!.response;

      //update the state after success
      state.maybeMap(
        orElse: () {},
        mainState: (state) {
          final List<OrderModel> newOrders = List.from(state.myOrders)
            ..add(newOrder);

          final newState = state.copyWith(
            myOrders: newOrders,
            errorMessage: '',
          );

          emit(newState);
        },
      );
    } else {
      errorFun(errorMessage: result.failure!.message, emit: emit);
    }
  }

//for delete order and my recipe from the tables
  FutureOr<void> _onRecipesDeleteOrderEvent(
      RecipesDeleteOrderEvent event, Emitter<RecipesState> emit) async {
    final mainState = state as RecipesMainState;
    final order = event.order;
    //if the order is a privet order then delete myRecipe from the tables
    if (order.is_privet_order) {
      debugPrint('DeleteOrder: order is privet');
      var result = await HandleException.handle(
          () => apiServices.deleteMyRecipe(order.recipe_id));
      if (result.success != null) {
        debugPrint('DeleteOrder: order is deleted from server');

        await _deleteOrderFun(mainState, order, emit);
      } else {
        debugPrint('DeleteOrder: Error with delete order');

        errorFun(errorMessage: result.failure!.message, emit: emit);
      }
    } else {
      // or just delete the order
      debugPrint('DeleteOrder: order is from market');

      await _deleteOrderFun(mainState, order, emit);
    }
  }

// for delete order from order table
  FutureOr<void> _deleteOrderFun(RecipesMainState state, OrderModel order,
      Emitter<RecipesState> emit) async {
    await HandleException.handle(() => apiServices.deleteOrder(order.objectId!))
        .then((result) {
      //update the state after success
      if (result.success != null) {
        debugPrint('DeleteOrder: the order is deleted from server');

        final List<OrderModel> newOrders = List.from(state.myOrders)
          ..remove(order);

        final List<RecipeModel> newMyRecipes = List.from(state.myRecipes)
          ..removeWhere((recipe) => recipe.objectId == order.recipe_id);
        final List<RecipeModel> newOrderScreenList =
            List.from(state.orderScreenList)
              ..removeWhere((recipe) => recipe.objectId == order.recipe_id);

        final newState = state.copyWith(
          myOrders: newOrders,
          myRecipes: newMyRecipes,
          orderScreenList: newOrderScreenList,
          errorMessage: '',
        );
        debugPrint('DeleteOrder: The state is updated');
        emit(newState);
      } else {
        debugPrint('DeleteOrder: Error with delete my recipe');

        errorFun(errorMessage: result.failure!.message, emit: emit);
      }
    });
  }

//chang the list in order screen
  _onFillOrdersListEvent(
      RecipesFillOrdersListEvent event, Emitter<RecipesState> emit) async {
    final newState = state.maybeMap(
        orElse: () => const RecipesState.mainState(),
        mainState: (state) {
          if (state.isMyRecipeList) {
            //choosing the market recipes

            final orders = state.myOrders
                .where((order) => !order.is_privet_order)
                .toList();
            List<RecipeModel> recipesList = List.generate(
                orders.length,
                (index) => state.recipes
                    .where(
                        (recipe) => recipe.objectId == orders[index].recipe_id)
                    .first);

            return state.copyWith(
                orderScreenList: recipesList,
                isMyRecipeList: !state.isMyRecipeList);
          } else {
            //choosing user recipes

            return state.copyWith(isMyRecipeList: !state.isMyRecipeList);
          }
        });
    emit(newState);
  }

// update error message
  _onRecipesErrorEvent(
      RecipesErrorEvent event, Emitter<RecipesState> emit) async {
    errorFun(errorMessage: event.error, emit: emit);
  }

  errorFun(
      {required String errorMessage, required Emitter<RecipesState> emit}) {
    debugPrint('Error: $errorMessage');
    final newState = state.maybeMap(
        orElse: () => const RecipesState.mainState(),
        mainState: (state) => state.copyWith(errorMessage: errorMessage));
    emit(newState);
  }
}
