// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'recipes_bloc.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_RecipesInitialState _$$_RecipesInitialStateFromJson(
        Map<String, dynamic> json) =>
    _$_RecipesInitialState(
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$_RecipesInitialStateToJson(
        _$_RecipesInitialState instance) =>
    <String, dynamic>{
      'runtimeType': instance.$type,
    };

_$RecipesMainState _$$RecipesMainStateFromJson(Map<String, dynamic> json) =>
    _$RecipesMainState(
      recipes: (json['recipes'] as List<dynamic>?)
              ?.map((e) => RecipeModel.fromJson(e as Map<String, dynamic>))
              .toList() ??
          const [],
      myRecipes: (json['myRecipes'] as List<dynamic>?)
              ?.map((e) => RecipeModel.fromJson(e as Map<String, dynamic>))
              .toList() ??
          const [],
      restaurants: (json['restaurants'] as List<dynamic>?)
              ?.map((e) => RestaurantModel.fromJson(e as Map<String, dynamic>))
              .toList() ??
          const [],
      myOrders: (json['myOrders'] as List<dynamic>?)
              ?.map((e) => OrderModel.fromJson(e as Map<String, dynamic>))
              .toList() ??
          const [],
      errorMessage: json['errorMessage'] as String? ?? '',
      orderScreenList: (json['orderScreenList'] as List<dynamic>?)
              ?.map((e) => RecipeModel.fromJson(e as Map<String, dynamic>))
              .toList() ??
          const [],
      isMyRecipeList: json['isMyRecipeList'] as bool? ?? false,
      appLoadingStatus: $enumDecodeNullable(
              _$AppLoadingStatusEnumMap, json['appLoadingStatus']) ??
          AppLoadingStatus.loading,
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$RecipesMainStateToJson(_$RecipesMainState instance) =>
    <String, dynamic>{
      'recipes': instance.recipes,
      'myRecipes': instance.myRecipes,
      'restaurants': instance.restaurants,
      'myOrders': instance.myOrders,
      'errorMessage': instance.errorMessage,
      'orderScreenList': instance.orderScreenList,
      'isMyRecipeList': instance.isMyRecipeList,
      'appLoadingStatus': _$AppLoadingStatusEnumMap[instance.appLoadingStatus]!,
      'runtimeType': instance.$type,
    };

const _$AppLoadingStatusEnumMap = {
  AppLoadingStatus.loading: 'loading',
  AppLoadingStatus.done: 'done',
};
