// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'recipes_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$RecipesEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() fillOrdersList,
    required TResult Function(RecipeModel recipe) orderMyRecipe,
    required TResult Function(RecipeModel recipe) orderRecipe,
    required TResult Function(String error) showError,
    required TResult Function(OrderModel order) deleteOrder,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? fillOrdersList,
    TResult? Function(RecipeModel recipe)? orderMyRecipe,
    TResult? Function(RecipeModel recipe)? orderRecipe,
    TResult? Function(String error)? showError,
    TResult? Function(OrderModel order)? deleteOrder,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? fillOrdersList,
    TResult Function(RecipeModel recipe)? orderMyRecipe,
    TResult Function(RecipeModel recipe)? orderRecipe,
    TResult Function(String error)? showError,
    TResult Function(OrderModel order)? deleteOrder,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RecipesInitialEvent value) initial,
    required TResult Function(RecipesFillOrdersListEvent value) fillOrdersList,
    required TResult Function(RecipesOrderMyRecipeEvent value) orderMyRecipe,
    required TResult Function(RecipesOrderRecipeEvent value) orderRecipe,
    required TResult Function(RecipesErrorEvent value) showError,
    required TResult Function(RecipesDeleteOrderEvent value) deleteOrder,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(RecipesInitialEvent value)? initial,
    TResult? Function(RecipesFillOrdersListEvent value)? fillOrdersList,
    TResult? Function(RecipesOrderMyRecipeEvent value)? orderMyRecipe,
    TResult? Function(RecipesOrderRecipeEvent value)? orderRecipe,
    TResult? Function(RecipesErrorEvent value)? showError,
    TResult? Function(RecipesDeleteOrderEvent value)? deleteOrder,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RecipesInitialEvent value)? initial,
    TResult Function(RecipesFillOrdersListEvent value)? fillOrdersList,
    TResult Function(RecipesOrderMyRecipeEvent value)? orderMyRecipe,
    TResult Function(RecipesOrderRecipeEvent value)? orderRecipe,
    TResult Function(RecipesErrorEvent value)? showError,
    TResult Function(RecipesDeleteOrderEvent value)? deleteOrder,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $RecipesEventCopyWith<$Res> {
  factory $RecipesEventCopyWith(
          RecipesEvent value, $Res Function(RecipesEvent) then) =
      _$RecipesEventCopyWithImpl<$Res, RecipesEvent>;
}

/// @nodoc
class _$RecipesEventCopyWithImpl<$Res, $Val extends RecipesEvent>
    implements $RecipesEventCopyWith<$Res> {
  _$RecipesEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$RecipesInitialEventCopyWith<$Res> {
  factory _$$RecipesInitialEventCopyWith(_$RecipesInitialEvent value,
          $Res Function(_$RecipesInitialEvent) then) =
      __$$RecipesInitialEventCopyWithImpl<$Res>;
}

/// @nodoc
class __$$RecipesInitialEventCopyWithImpl<$Res>
    extends _$RecipesEventCopyWithImpl<$Res, _$RecipesInitialEvent>
    implements _$$RecipesInitialEventCopyWith<$Res> {
  __$$RecipesInitialEventCopyWithImpl(
      _$RecipesInitialEvent _value, $Res Function(_$RecipesInitialEvent) _then)
      : super(_value, _then);
}

/// @nodoc

class _$RecipesInitialEvent implements RecipesInitialEvent {
  const _$RecipesInitialEvent();

  @override
  String toString() {
    return 'RecipesEvent.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$RecipesInitialEvent);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() fillOrdersList,
    required TResult Function(RecipeModel recipe) orderMyRecipe,
    required TResult Function(RecipeModel recipe) orderRecipe,
    required TResult Function(String error) showError,
    required TResult Function(OrderModel order) deleteOrder,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? fillOrdersList,
    TResult? Function(RecipeModel recipe)? orderMyRecipe,
    TResult? Function(RecipeModel recipe)? orderRecipe,
    TResult? Function(String error)? showError,
    TResult? Function(OrderModel order)? deleteOrder,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? fillOrdersList,
    TResult Function(RecipeModel recipe)? orderMyRecipe,
    TResult Function(RecipeModel recipe)? orderRecipe,
    TResult Function(String error)? showError,
    TResult Function(OrderModel order)? deleteOrder,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RecipesInitialEvent value) initial,
    required TResult Function(RecipesFillOrdersListEvent value) fillOrdersList,
    required TResult Function(RecipesOrderMyRecipeEvent value) orderMyRecipe,
    required TResult Function(RecipesOrderRecipeEvent value) orderRecipe,
    required TResult Function(RecipesErrorEvent value) showError,
    required TResult Function(RecipesDeleteOrderEvent value) deleteOrder,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(RecipesInitialEvent value)? initial,
    TResult? Function(RecipesFillOrdersListEvent value)? fillOrdersList,
    TResult? Function(RecipesOrderMyRecipeEvent value)? orderMyRecipe,
    TResult? Function(RecipesOrderRecipeEvent value)? orderRecipe,
    TResult? Function(RecipesErrorEvent value)? showError,
    TResult? Function(RecipesDeleteOrderEvent value)? deleteOrder,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RecipesInitialEvent value)? initial,
    TResult Function(RecipesFillOrdersListEvent value)? fillOrdersList,
    TResult Function(RecipesOrderMyRecipeEvent value)? orderMyRecipe,
    TResult Function(RecipesOrderRecipeEvent value)? orderRecipe,
    TResult Function(RecipesErrorEvent value)? showError,
    TResult Function(RecipesDeleteOrderEvent value)? deleteOrder,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class RecipesInitialEvent implements RecipesEvent {
  const factory RecipesInitialEvent() = _$RecipesInitialEvent;
}

/// @nodoc
abstract class _$$RecipesFillOrdersListEventCopyWith<$Res> {
  factory _$$RecipesFillOrdersListEventCopyWith(
          _$RecipesFillOrdersListEvent value,
          $Res Function(_$RecipesFillOrdersListEvent) then) =
      __$$RecipesFillOrdersListEventCopyWithImpl<$Res>;
}

/// @nodoc
class __$$RecipesFillOrdersListEventCopyWithImpl<$Res>
    extends _$RecipesEventCopyWithImpl<$Res, _$RecipesFillOrdersListEvent>
    implements _$$RecipesFillOrdersListEventCopyWith<$Res> {
  __$$RecipesFillOrdersListEventCopyWithImpl(
      _$RecipesFillOrdersListEvent _value,
      $Res Function(_$RecipesFillOrdersListEvent) _then)
      : super(_value, _then);
}

/// @nodoc

class _$RecipesFillOrdersListEvent implements RecipesFillOrdersListEvent {
  const _$RecipesFillOrdersListEvent();

  @override
  String toString() {
    return 'RecipesEvent.fillOrdersList()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$RecipesFillOrdersListEvent);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() fillOrdersList,
    required TResult Function(RecipeModel recipe) orderMyRecipe,
    required TResult Function(RecipeModel recipe) orderRecipe,
    required TResult Function(String error) showError,
    required TResult Function(OrderModel order) deleteOrder,
  }) {
    return fillOrdersList();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? fillOrdersList,
    TResult? Function(RecipeModel recipe)? orderMyRecipe,
    TResult? Function(RecipeModel recipe)? orderRecipe,
    TResult? Function(String error)? showError,
    TResult? Function(OrderModel order)? deleteOrder,
  }) {
    return fillOrdersList?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? fillOrdersList,
    TResult Function(RecipeModel recipe)? orderMyRecipe,
    TResult Function(RecipeModel recipe)? orderRecipe,
    TResult Function(String error)? showError,
    TResult Function(OrderModel order)? deleteOrder,
    required TResult orElse(),
  }) {
    if (fillOrdersList != null) {
      return fillOrdersList();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RecipesInitialEvent value) initial,
    required TResult Function(RecipesFillOrdersListEvent value) fillOrdersList,
    required TResult Function(RecipesOrderMyRecipeEvent value) orderMyRecipe,
    required TResult Function(RecipesOrderRecipeEvent value) orderRecipe,
    required TResult Function(RecipesErrorEvent value) showError,
    required TResult Function(RecipesDeleteOrderEvent value) deleteOrder,
  }) {
    return fillOrdersList(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(RecipesInitialEvent value)? initial,
    TResult? Function(RecipesFillOrdersListEvent value)? fillOrdersList,
    TResult? Function(RecipesOrderMyRecipeEvent value)? orderMyRecipe,
    TResult? Function(RecipesOrderRecipeEvent value)? orderRecipe,
    TResult? Function(RecipesErrorEvent value)? showError,
    TResult? Function(RecipesDeleteOrderEvent value)? deleteOrder,
  }) {
    return fillOrdersList?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RecipesInitialEvent value)? initial,
    TResult Function(RecipesFillOrdersListEvent value)? fillOrdersList,
    TResult Function(RecipesOrderMyRecipeEvent value)? orderMyRecipe,
    TResult Function(RecipesOrderRecipeEvent value)? orderRecipe,
    TResult Function(RecipesErrorEvent value)? showError,
    TResult Function(RecipesDeleteOrderEvent value)? deleteOrder,
    required TResult orElse(),
  }) {
    if (fillOrdersList != null) {
      return fillOrdersList(this);
    }
    return orElse();
  }
}

abstract class RecipesFillOrdersListEvent implements RecipesEvent {
  const factory RecipesFillOrdersListEvent() = _$RecipesFillOrdersListEvent;
}

/// @nodoc
abstract class _$$RecipesOrderMyRecipeEventCopyWith<$Res> {
  factory _$$RecipesOrderMyRecipeEventCopyWith(
          _$RecipesOrderMyRecipeEvent value,
          $Res Function(_$RecipesOrderMyRecipeEvent) then) =
      __$$RecipesOrderMyRecipeEventCopyWithImpl<$Res>;
  @useResult
  $Res call({RecipeModel recipe});

  $RecipeModelCopyWith<$Res> get recipe;
}

/// @nodoc
class __$$RecipesOrderMyRecipeEventCopyWithImpl<$Res>
    extends _$RecipesEventCopyWithImpl<$Res, _$RecipesOrderMyRecipeEvent>
    implements _$$RecipesOrderMyRecipeEventCopyWith<$Res> {
  __$$RecipesOrderMyRecipeEventCopyWithImpl(_$RecipesOrderMyRecipeEvent _value,
      $Res Function(_$RecipesOrderMyRecipeEvent) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? recipe = null,
  }) {
    return _then(_$RecipesOrderMyRecipeEvent(
      recipe: null == recipe
          ? _value.recipe
          : recipe // ignore: cast_nullable_to_non_nullable
              as RecipeModel,
    ));
  }

  @override
  @pragma('vm:prefer-inline')
  $RecipeModelCopyWith<$Res> get recipe {
    return $RecipeModelCopyWith<$Res>(_value.recipe, (value) {
      return _then(_value.copyWith(recipe: value));
    });
  }
}

/// @nodoc

class _$RecipesOrderMyRecipeEvent implements RecipesOrderMyRecipeEvent {
  const _$RecipesOrderMyRecipeEvent({required this.recipe});

  @override
  final RecipeModel recipe;

  @override
  String toString() {
    return 'RecipesEvent.orderMyRecipe(recipe: $recipe)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$RecipesOrderMyRecipeEvent &&
            (identical(other.recipe, recipe) || other.recipe == recipe));
  }

  @override
  int get hashCode => Object.hash(runtimeType, recipe);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$RecipesOrderMyRecipeEventCopyWith<_$RecipesOrderMyRecipeEvent>
      get copyWith => __$$RecipesOrderMyRecipeEventCopyWithImpl<
          _$RecipesOrderMyRecipeEvent>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() fillOrdersList,
    required TResult Function(RecipeModel recipe) orderMyRecipe,
    required TResult Function(RecipeModel recipe) orderRecipe,
    required TResult Function(String error) showError,
    required TResult Function(OrderModel order) deleteOrder,
  }) {
    return orderMyRecipe(recipe);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? fillOrdersList,
    TResult? Function(RecipeModel recipe)? orderMyRecipe,
    TResult? Function(RecipeModel recipe)? orderRecipe,
    TResult? Function(String error)? showError,
    TResult? Function(OrderModel order)? deleteOrder,
  }) {
    return orderMyRecipe?.call(recipe);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? fillOrdersList,
    TResult Function(RecipeModel recipe)? orderMyRecipe,
    TResult Function(RecipeModel recipe)? orderRecipe,
    TResult Function(String error)? showError,
    TResult Function(OrderModel order)? deleteOrder,
    required TResult orElse(),
  }) {
    if (orderMyRecipe != null) {
      return orderMyRecipe(recipe);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RecipesInitialEvent value) initial,
    required TResult Function(RecipesFillOrdersListEvent value) fillOrdersList,
    required TResult Function(RecipesOrderMyRecipeEvent value) orderMyRecipe,
    required TResult Function(RecipesOrderRecipeEvent value) orderRecipe,
    required TResult Function(RecipesErrorEvent value) showError,
    required TResult Function(RecipesDeleteOrderEvent value) deleteOrder,
  }) {
    return orderMyRecipe(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(RecipesInitialEvent value)? initial,
    TResult? Function(RecipesFillOrdersListEvent value)? fillOrdersList,
    TResult? Function(RecipesOrderMyRecipeEvent value)? orderMyRecipe,
    TResult? Function(RecipesOrderRecipeEvent value)? orderRecipe,
    TResult? Function(RecipesErrorEvent value)? showError,
    TResult? Function(RecipesDeleteOrderEvent value)? deleteOrder,
  }) {
    return orderMyRecipe?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RecipesInitialEvent value)? initial,
    TResult Function(RecipesFillOrdersListEvent value)? fillOrdersList,
    TResult Function(RecipesOrderMyRecipeEvent value)? orderMyRecipe,
    TResult Function(RecipesOrderRecipeEvent value)? orderRecipe,
    TResult Function(RecipesErrorEvent value)? showError,
    TResult Function(RecipesDeleteOrderEvent value)? deleteOrder,
    required TResult orElse(),
  }) {
    if (orderMyRecipe != null) {
      return orderMyRecipe(this);
    }
    return orElse();
  }
}

abstract class RecipesOrderMyRecipeEvent implements RecipesEvent {
  const factory RecipesOrderMyRecipeEvent({required final RecipeModel recipe}) =
      _$RecipesOrderMyRecipeEvent;

  RecipeModel get recipe;
  @JsonKey(ignore: true)
  _$$RecipesOrderMyRecipeEventCopyWith<_$RecipesOrderMyRecipeEvent>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$RecipesOrderRecipeEventCopyWith<$Res> {
  factory _$$RecipesOrderRecipeEventCopyWith(_$RecipesOrderRecipeEvent value,
          $Res Function(_$RecipesOrderRecipeEvent) then) =
      __$$RecipesOrderRecipeEventCopyWithImpl<$Res>;
  @useResult
  $Res call({RecipeModel recipe});

  $RecipeModelCopyWith<$Res> get recipe;
}

/// @nodoc
class __$$RecipesOrderRecipeEventCopyWithImpl<$Res>
    extends _$RecipesEventCopyWithImpl<$Res, _$RecipesOrderRecipeEvent>
    implements _$$RecipesOrderRecipeEventCopyWith<$Res> {
  __$$RecipesOrderRecipeEventCopyWithImpl(_$RecipesOrderRecipeEvent _value,
      $Res Function(_$RecipesOrderRecipeEvent) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? recipe = null,
  }) {
    return _then(_$RecipesOrderRecipeEvent(
      recipe: null == recipe
          ? _value.recipe
          : recipe // ignore: cast_nullable_to_non_nullable
              as RecipeModel,
    ));
  }

  @override
  @pragma('vm:prefer-inline')
  $RecipeModelCopyWith<$Res> get recipe {
    return $RecipeModelCopyWith<$Res>(_value.recipe, (value) {
      return _then(_value.copyWith(recipe: value));
    });
  }
}

/// @nodoc

class _$RecipesOrderRecipeEvent implements RecipesOrderRecipeEvent {
  const _$RecipesOrderRecipeEvent({required this.recipe});

  @override
  final RecipeModel recipe;

  @override
  String toString() {
    return 'RecipesEvent.orderRecipe(recipe: $recipe)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$RecipesOrderRecipeEvent &&
            (identical(other.recipe, recipe) || other.recipe == recipe));
  }

  @override
  int get hashCode => Object.hash(runtimeType, recipe);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$RecipesOrderRecipeEventCopyWith<_$RecipesOrderRecipeEvent> get copyWith =>
      __$$RecipesOrderRecipeEventCopyWithImpl<_$RecipesOrderRecipeEvent>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() fillOrdersList,
    required TResult Function(RecipeModel recipe) orderMyRecipe,
    required TResult Function(RecipeModel recipe) orderRecipe,
    required TResult Function(String error) showError,
    required TResult Function(OrderModel order) deleteOrder,
  }) {
    return orderRecipe(recipe);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? fillOrdersList,
    TResult? Function(RecipeModel recipe)? orderMyRecipe,
    TResult? Function(RecipeModel recipe)? orderRecipe,
    TResult? Function(String error)? showError,
    TResult? Function(OrderModel order)? deleteOrder,
  }) {
    return orderRecipe?.call(recipe);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? fillOrdersList,
    TResult Function(RecipeModel recipe)? orderMyRecipe,
    TResult Function(RecipeModel recipe)? orderRecipe,
    TResult Function(String error)? showError,
    TResult Function(OrderModel order)? deleteOrder,
    required TResult orElse(),
  }) {
    if (orderRecipe != null) {
      return orderRecipe(recipe);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RecipesInitialEvent value) initial,
    required TResult Function(RecipesFillOrdersListEvent value) fillOrdersList,
    required TResult Function(RecipesOrderMyRecipeEvent value) orderMyRecipe,
    required TResult Function(RecipesOrderRecipeEvent value) orderRecipe,
    required TResult Function(RecipesErrorEvent value) showError,
    required TResult Function(RecipesDeleteOrderEvent value) deleteOrder,
  }) {
    return orderRecipe(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(RecipesInitialEvent value)? initial,
    TResult? Function(RecipesFillOrdersListEvent value)? fillOrdersList,
    TResult? Function(RecipesOrderMyRecipeEvent value)? orderMyRecipe,
    TResult? Function(RecipesOrderRecipeEvent value)? orderRecipe,
    TResult? Function(RecipesErrorEvent value)? showError,
    TResult? Function(RecipesDeleteOrderEvent value)? deleteOrder,
  }) {
    return orderRecipe?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RecipesInitialEvent value)? initial,
    TResult Function(RecipesFillOrdersListEvent value)? fillOrdersList,
    TResult Function(RecipesOrderMyRecipeEvent value)? orderMyRecipe,
    TResult Function(RecipesOrderRecipeEvent value)? orderRecipe,
    TResult Function(RecipesErrorEvent value)? showError,
    TResult Function(RecipesDeleteOrderEvent value)? deleteOrder,
    required TResult orElse(),
  }) {
    if (orderRecipe != null) {
      return orderRecipe(this);
    }
    return orElse();
  }
}

abstract class RecipesOrderRecipeEvent implements RecipesEvent {
  const factory RecipesOrderRecipeEvent({required final RecipeModel recipe}) =
      _$RecipesOrderRecipeEvent;

  RecipeModel get recipe;
  @JsonKey(ignore: true)
  _$$RecipesOrderRecipeEventCopyWith<_$RecipesOrderRecipeEvent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$RecipesErrorEventCopyWith<$Res> {
  factory _$$RecipesErrorEventCopyWith(
          _$RecipesErrorEvent value, $Res Function(_$RecipesErrorEvent) then) =
      __$$RecipesErrorEventCopyWithImpl<$Res>;
  @useResult
  $Res call({String error});
}

/// @nodoc
class __$$RecipesErrorEventCopyWithImpl<$Res>
    extends _$RecipesEventCopyWithImpl<$Res, _$RecipesErrorEvent>
    implements _$$RecipesErrorEventCopyWith<$Res> {
  __$$RecipesErrorEventCopyWithImpl(
      _$RecipesErrorEvent _value, $Res Function(_$RecipesErrorEvent) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? error = null,
  }) {
    return _then(_$RecipesErrorEvent(
      null == error
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$RecipesErrorEvent implements RecipesErrorEvent {
  const _$RecipesErrorEvent(this.error);

  @override
  final String error;

  @override
  String toString() {
    return 'RecipesEvent.showError(error: $error)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$RecipesErrorEvent &&
            (identical(other.error, error) || other.error == error));
  }

  @override
  int get hashCode => Object.hash(runtimeType, error);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$RecipesErrorEventCopyWith<_$RecipesErrorEvent> get copyWith =>
      __$$RecipesErrorEventCopyWithImpl<_$RecipesErrorEvent>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() fillOrdersList,
    required TResult Function(RecipeModel recipe) orderMyRecipe,
    required TResult Function(RecipeModel recipe) orderRecipe,
    required TResult Function(String error) showError,
    required TResult Function(OrderModel order) deleteOrder,
  }) {
    return showError(error);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? fillOrdersList,
    TResult? Function(RecipeModel recipe)? orderMyRecipe,
    TResult? Function(RecipeModel recipe)? orderRecipe,
    TResult? Function(String error)? showError,
    TResult? Function(OrderModel order)? deleteOrder,
  }) {
    return showError?.call(error);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? fillOrdersList,
    TResult Function(RecipeModel recipe)? orderMyRecipe,
    TResult Function(RecipeModel recipe)? orderRecipe,
    TResult Function(String error)? showError,
    TResult Function(OrderModel order)? deleteOrder,
    required TResult orElse(),
  }) {
    if (showError != null) {
      return showError(error);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RecipesInitialEvent value) initial,
    required TResult Function(RecipesFillOrdersListEvent value) fillOrdersList,
    required TResult Function(RecipesOrderMyRecipeEvent value) orderMyRecipe,
    required TResult Function(RecipesOrderRecipeEvent value) orderRecipe,
    required TResult Function(RecipesErrorEvent value) showError,
    required TResult Function(RecipesDeleteOrderEvent value) deleteOrder,
  }) {
    return showError(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(RecipesInitialEvent value)? initial,
    TResult? Function(RecipesFillOrdersListEvent value)? fillOrdersList,
    TResult? Function(RecipesOrderMyRecipeEvent value)? orderMyRecipe,
    TResult? Function(RecipesOrderRecipeEvent value)? orderRecipe,
    TResult? Function(RecipesErrorEvent value)? showError,
    TResult? Function(RecipesDeleteOrderEvent value)? deleteOrder,
  }) {
    return showError?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RecipesInitialEvent value)? initial,
    TResult Function(RecipesFillOrdersListEvent value)? fillOrdersList,
    TResult Function(RecipesOrderMyRecipeEvent value)? orderMyRecipe,
    TResult Function(RecipesOrderRecipeEvent value)? orderRecipe,
    TResult Function(RecipesErrorEvent value)? showError,
    TResult Function(RecipesDeleteOrderEvent value)? deleteOrder,
    required TResult orElse(),
  }) {
    if (showError != null) {
      return showError(this);
    }
    return orElse();
  }
}

abstract class RecipesErrorEvent implements RecipesEvent {
  const factory RecipesErrorEvent(final String error) = _$RecipesErrorEvent;

  String get error;
  @JsonKey(ignore: true)
  _$$RecipesErrorEventCopyWith<_$RecipesErrorEvent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$RecipesDeleteOrderEventCopyWith<$Res> {
  factory _$$RecipesDeleteOrderEventCopyWith(_$RecipesDeleteOrderEvent value,
          $Res Function(_$RecipesDeleteOrderEvent) then) =
      __$$RecipesDeleteOrderEventCopyWithImpl<$Res>;
  @useResult
  $Res call({OrderModel order});

  $OrderModelCopyWith<$Res> get order;
}

/// @nodoc
class __$$RecipesDeleteOrderEventCopyWithImpl<$Res>
    extends _$RecipesEventCopyWithImpl<$Res, _$RecipesDeleteOrderEvent>
    implements _$$RecipesDeleteOrderEventCopyWith<$Res> {
  __$$RecipesDeleteOrderEventCopyWithImpl(_$RecipesDeleteOrderEvent _value,
      $Res Function(_$RecipesDeleteOrderEvent) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? order = null,
  }) {
    return _then(_$RecipesDeleteOrderEvent(
      null == order
          ? _value.order
          : order // ignore: cast_nullable_to_non_nullable
              as OrderModel,
    ));
  }

  @override
  @pragma('vm:prefer-inline')
  $OrderModelCopyWith<$Res> get order {
    return $OrderModelCopyWith<$Res>(_value.order, (value) {
      return _then(_value.copyWith(order: value));
    });
  }
}

/// @nodoc

class _$RecipesDeleteOrderEvent implements RecipesDeleteOrderEvent {
  const _$RecipesDeleteOrderEvent(this.order);

  @override
  final OrderModel order;

  @override
  String toString() {
    return 'RecipesEvent.deleteOrder(order: $order)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$RecipesDeleteOrderEvent &&
            (identical(other.order, order) || other.order == order));
  }

  @override
  int get hashCode => Object.hash(runtimeType, order);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$RecipesDeleteOrderEventCopyWith<_$RecipesDeleteOrderEvent> get copyWith =>
      __$$RecipesDeleteOrderEventCopyWithImpl<_$RecipesDeleteOrderEvent>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() fillOrdersList,
    required TResult Function(RecipeModel recipe) orderMyRecipe,
    required TResult Function(RecipeModel recipe) orderRecipe,
    required TResult Function(String error) showError,
    required TResult Function(OrderModel order) deleteOrder,
  }) {
    return deleteOrder(order);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? fillOrdersList,
    TResult? Function(RecipeModel recipe)? orderMyRecipe,
    TResult? Function(RecipeModel recipe)? orderRecipe,
    TResult? Function(String error)? showError,
    TResult? Function(OrderModel order)? deleteOrder,
  }) {
    return deleteOrder?.call(order);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? fillOrdersList,
    TResult Function(RecipeModel recipe)? orderMyRecipe,
    TResult Function(RecipeModel recipe)? orderRecipe,
    TResult Function(String error)? showError,
    TResult Function(OrderModel order)? deleteOrder,
    required TResult orElse(),
  }) {
    if (deleteOrder != null) {
      return deleteOrder(order);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RecipesInitialEvent value) initial,
    required TResult Function(RecipesFillOrdersListEvent value) fillOrdersList,
    required TResult Function(RecipesOrderMyRecipeEvent value) orderMyRecipe,
    required TResult Function(RecipesOrderRecipeEvent value) orderRecipe,
    required TResult Function(RecipesErrorEvent value) showError,
    required TResult Function(RecipesDeleteOrderEvent value) deleteOrder,
  }) {
    return deleteOrder(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(RecipesInitialEvent value)? initial,
    TResult? Function(RecipesFillOrdersListEvent value)? fillOrdersList,
    TResult? Function(RecipesOrderMyRecipeEvent value)? orderMyRecipe,
    TResult? Function(RecipesOrderRecipeEvent value)? orderRecipe,
    TResult? Function(RecipesErrorEvent value)? showError,
    TResult? Function(RecipesDeleteOrderEvent value)? deleteOrder,
  }) {
    return deleteOrder?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RecipesInitialEvent value)? initial,
    TResult Function(RecipesFillOrdersListEvent value)? fillOrdersList,
    TResult Function(RecipesOrderMyRecipeEvent value)? orderMyRecipe,
    TResult Function(RecipesOrderRecipeEvent value)? orderRecipe,
    TResult Function(RecipesErrorEvent value)? showError,
    TResult Function(RecipesDeleteOrderEvent value)? deleteOrder,
    required TResult orElse(),
  }) {
    if (deleteOrder != null) {
      return deleteOrder(this);
    }
    return orElse();
  }
}

abstract class RecipesDeleteOrderEvent implements RecipesEvent {
  const factory RecipesDeleteOrderEvent(final OrderModel order) =
      _$RecipesDeleteOrderEvent;

  OrderModel get order;
  @JsonKey(ignore: true)
  _$$RecipesDeleteOrderEventCopyWith<_$RecipesDeleteOrderEvent> get copyWith =>
      throw _privateConstructorUsedError;
}

RecipesState _$RecipesStateFromJson(Map<String, dynamic> json) {
  switch (json['runtimeType']) {
    case 'initialState':
      return _RecipesInitialState.fromJson(json);
    case 'mainState':
      return RecipesMainState.fromJson(json);

    default:
      throw CheckedFromJsonException(json, 'runtimeType', 'RecipesState',
          'Invalid union type "${json['runtimeType']}"!');
  }
}

/// @nodoc
mixin _$RecipesState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initialState,
    required TResult Function(
            List<RecipeModel> recipes,
            List<RecipeModel> myRecipes,
            List<RestaurantModel> restaurants,
            List<OrderModel> myOrders,
            String errorMessage,
            List<RecipeModel> orderScreenList,
            bool isMyRecipeList,
            AppLoadingStatus appLoadingStatus)
        mainState,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initialState,
    TResult? Function(
            List<RecipeModel> recipes,
            List<RecipeModel> myRecipes,
            List<RestaurantModel> restaurants,
            List<OrderModel> myOrders,
            String errorMessage,
            List<RecipeModel> orderScreenList,
            bool isMyRecipeList,
            AppLoadingStatus appLoadingStatus)?
        mainState,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initialState,
    TResult Function(
            List<RecipeModel> recipes,
            List<RecipeModel> myRecipes,
            List<RestaurantModel> restaurants,
            List<OrderModel> myOrders,
            String errorMessage,
            List<RecipeModel> orderScreenList,
            bool isMyRecipeList,
            AppLoadingStatus appLoadingStatus)?
        mainState,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_RecipesInitialState value) initialState,
    required TResult Function(RecipesMainState value) mainState,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_RecipesInitialState value)? initialState,
    TResult? Function(RecipesMainState value)? mainState,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_RecipesInitialState value)? initialState,
    TResult Function(RecipesMainState value)? mainState,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $RecipesStateCopyWith<$Res> {
  factory $RecipesStateCopyWith(
          RecipesState value, $Res Function(RecipesState) then) =
      _$RecipesStateCopyWithImpl<$Res, RecipesState>;
}

/// @nodoc
class _$RecipesStateCopyWithImpl<$Res, $Val extends RecipesState>
    implements $RecipesStateCopyWith<$Res> {
  _$RecipesStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_RecipesInitialStateCopyWith<$Res> {
  factory _$$_RecipesInitialStateCopyWith(_$_RecipesInitialState value,
          $Res Function(_$_RecipesInitialState) then) =
      __$$_RecipesInitialStateCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_RecipesInitialStateCopyWithImpl<$Res>
    extends _$RecipesStateCopyWithImpl<$Res, _$_RecipesInitialState>
    implements _$$_RecipesInitialStateCopyWith<$Res> {
  __$$_RecipesInitialStateCopyWithImpl(_$_RecipesInitialState _value,
      $Res Function(_$_RecipesInitialState) _then)
      : super(_value, _then);
}

/// @nodoc
@JsonSerializable()
class _$_RecipesInitialState extends _RecipesInitialState {
  const _$_RecipesInitialState({final String? $type})
      : $type = $type ?? 'initialState',
        super._();

  factory _$_RecipesInitialState.fromJson(Map<String, dynamic> json) =>
      _$$_RecipesInitialStateFromJson(json);

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'RecipesState.initialState()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_RecipesInitialState);
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initialState,
    required TResult Function(
            List<RecipeModel> recipes,
            List<RecipeModel> myRecipes,
            List<RestaurantModel> restaurants,
            List<OrderModel> myOrders,
            String errorMessage,
            List<RecipeModel> orderScreenList,
            bool isMyRecipeList,
            AppLoadingStatus appLoadingStatus)
        mainState,
  }) {
    return initialState();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initialState,
    TResult? Function(
            List<RecipeModel> recipes,
            List<RecipeModel> myRecipes,
            List<RestaurantModel> restaurants,
            List<OrderModel> myOrders,
            String errorMessage,
            List<RecipeModel> orderScreenList,
            bool isMyRecipeList,
            AppLoadingStatus appLoadingStatus)?
        mainState,
  }) {
    return initialState?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initialState,
    TResult Function(
            List<RecipeModel> recipes,
            List<RecipeModel> myRecipes,
            List<RestaurantModel> restaurants,
            List<OrderModel> myOrders,
            String errorMessage,
            List<RecipeModel> orderScreenList,
            bool isMyRecipeList,
            AppLoadingStatus appLoadingStatus)?
        mainState,
    required TResult orElse(),
  }) {
    if (initialState != null) {
      return initialState();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_RecipesInitialState value) initialState,
    required TResult Function(RecipesMainState value) mainState,
  }) {
    return initialState(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_RecipesInitialState value)? initialState,
    TResult? Function(RecipesMainState value)? mainState,
  }) {
    return initialState?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_RecipesInitialState value)? initialState,
    TResult Function(RecipesMainState value)? mainState,
    required TResult orElse(),
  }) {
    if (initialState != null) {
      return initialState(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$_RecipesInitialStateToJson(
      this,
    );
  }
}

abstract class _RecipesInitialState extends RecipesState {
  const factory _RecipesInitialState() = _$_RecipesInitialState;
  const _RecipesInitialState._() : super._();

  factory _RecipesInitialState.fromJson(Map<String, dynamic> json) =
      _$_RecipesInitialState.fromJson;
}

/// @nodoc
abstract class _$$RecipesMainStateCopyWith<$Res> {
  factory _$$RecipesMainStateCopyWith(
          _$RecipesMainState value, $Res Function(_$RecipesMainState) then) =
      __$$RecipesMainStateCopyWithImpl<$Res>;
  @useResult
  $Res call(
      {List<RecipeModel> recipes,
      List<RecipeModel> myRecipes,
      List<RestaurantModel> restaurants,
      List<OrderModel> myOrders,
      String errorMessage,
      List<RecipeModel> orderScreenList,
      bool isMyRecipeList,
      AppLoadingStatus appLoadingStatus});
}

/// @nodoc
class __$$RecipesMainStateCopyWithImpl<$Res>
    extends _$RecipesStateCopyWithImpl<$Res, _$RecipesMainState>
    implements _$$RecipesMainStateCopyWith<$Res> {
  __$$RecipesMainStateCopyWithImpl(
      _$RecipesMainState _value, $Res Function(_$RecipesMainState) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? recipes = null,
    Object? myRecipes = null,
    Object? restaurants = null,
    Object? myOrders = null,
    Object? errorMessage = null,
    Object? orderScreenList = null,
    Object? isMyRecipeList = null,
    Object? appLoadingStatus = null,
  }) {
    return _then(_$RecipesMainState(
      recipes: null == recipes
          ? _value._recipes
          : recipes // ignore: cast_nullable_to_non_nullable
              as List<RecipeModel>,
      myRecipes: null == myRecipes
          ? _value._myRecipes
          : myRecipes // ignore: cast_nullable_to_non_nullable
              as List<RecipeModel>,
      restaurants: null == restaurants
          ? _value._restaurants
          : restaurants // ignore: cast_nullable_to_non_nullable
              as List<RestaurantModel>,
      myOrders: null == myOrders
          ? _value._myOrders
          : myOrders // ignore: cast_nullable_to_non_nullable
              as List<OrderModel>,
      errorMessage: null == errorMessage
          ? _value.errorMessage
          : errorMessage // ignore: cast_nullable_to_non_nullable
              as String,
      orderScreenList: null == orderScreenList
          ? _value._orderScreenList
          : orderScreenList // ignore: cast_nullable_to_non_nullable
              as List<RecipeModel>,
      isMyRecipeList: null == isMyRecipeList
          ? _value.isMyRecipeList
          : isMyRecipeList // ignore: cast_nullable_to_non_nullable
              as bool,
      appLoadingStatus: null == appLoadingStatus
          ? _value.appLoadingStatus
          : appLoadingStatus // ignore: cast_nullable_to_non_nullable
              as AppLoadingStatus,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$RecipesMainState extends RecipesMainState {
  const _$RecipesMainState(
      {final List<RecipeModel> recipes = const [],
      final List<RecipeModel> myRecipes = const [],
      final List<RestaurantModel> restaurants = const [],
      final List<OrderModel> myOrders = const [],
      this.errorMessage = '',
      final List<RecipeModel> orderScreenList = const [],
      this.isMyRecipeList = false,
      this.appLoadingStatus = AppLoadingStatus.loading,
      final String? $type})
      : _recipes = recipes,
        _myRecipes = myRecipes,
        _restaurants = restaurants,
        _myOrders = myOrders,
        _orderScreenList = orderScreenList,
        $type = $type ?? 'mainState',
        super._();

  factory _$RecipesMainState.fromJson(Map<String, dynamic> json) =>
      _$$RecipesMainStateFromJson(json);

//the market recipe
  final List<RecipeModel> _recipes;
//the market recipe
  @override
  @JsonKey()
  List<RecipeModel> get recipes {
    if (_recipes is EqualUnmodifiableListView) return _recipes;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_recipes);
  }

// the user recipes
  final List<RecipeModel> _myRecipes;
// the user recipes
  @override
  @JsonKey()
  List<RecipeModel> get myRecipes {
    if (_myRecipes is EqualUnmodifiableListView) return _myRecipes;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_myRecipes);
  }

//all restaurants list
  final List<RestaurantModel> _restaurants;
//all restaurants list
  @override
  @JsonKey()
  List<RestaurantModel> get restaurants {
    if (_restaurants is EqualUnmodifiableListView) return _restaurants;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_restaurants);
  }

//list of all orders
  final List<OrderModel> _myOrders;
//list of all orders
  @override
  @JsonKey()
  List<OrderModel> get myOrders {
    if (_myOrders is EqualUnmodifiableListView) return _myOrders;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_myOrders);
  }

  @override
  @JsonKey()
  final String errorMessage;
//for thr orders screen to choose witch orders will view
  final List<RecipeModel> _orderScreenList;
//for thr orders screen to choose witch orders will view
  @override
  @JsonKey()
  List<RecipeModel> get orderScreenList {
    if (_orderScreenList is EqualUnmodifiableListView) return _orderScreenList;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_orderScreenList);
  }

  @override
  @JsonKey()
  final bool isMyRecipeList;
// if the app is loading data or not
  @override
  @JsonKey()
  final AppLoadingStatus appLoadingStatus;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'RecipesState.mainState(recipes: $recipes, myRecipes: $myRecipes, restaurants: $restaurants, myOrders: $myOrders, errorMessage: $errorMessage, orderScreenList: $orderScreenList, isMyRecipeList: $isMyRecipeList, appLoadingStatus: $appLoadingStatus)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$RecipesMainState &&
            const DeepCollectionEquality().equals(other._recipes, _recipes) &&
            const DeepCollectionEquality()
                .equals(other._myRecipes, _myRecipes) &&
            const DeepCollectionEquality()
                .equals(other._restaurants, _restaurants) &&
            const DeepCollectionEquality().equals(other._myOrders, _myOrders) &&
            (identical(other.errorMessage, errorMessage) ||
                other.errorMessage == errorMessage) &&
            const DeepCollectionEquality()
                .equals(other._orderScreenList, _orderScreenList) &&
            (identical(other.isMyRecipeList, isMyRecipeList) ||
                other.isMyRecipeList == isMyRecipeList) &&
            (identical(other.appLoadingStatus, appLoadingStatus) ||
                other.appLoadingStatus == appLoadingStatus));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(_recipes),
      const DeepCollectionEquality().hash(_myRecipes),
      const DeepCollectionEquality().hash(_restaurants),
      const DeepCollectionEquality().hash(_myOrders),
      errorMessage,
      const DeepCollectionEquality().hash(_orderScreenList),
      isMyRecipeList,
      appLoadingStatus);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$RecipesMainStateCopyWith<_$RecipesMainState> get copyWith =>
      __$$RecipesMainStateCopyWithImpl<_$RecipesMainState>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initialState,
    required TResult Function(
            List<RecipeModel> recipes,
            List<RecipeModel> myRecipes,
            List<RestaurantModel> restaurants,
            List<OrderModel> myOrders,
            String errorMessage,
            List<RecipeModel> orderScreenList,
            bool isMyRecipeList,
            AppLoadingStatus appLoadingStatus)
        mainState,
  }) {
    return mainState(recipes, myRecipes, restaurants, myOrders, errorMessage,
        orderScreenList, isMyRecipeList, appLoadingStatus);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initialState,
    TResult? Function(
            List<RecipeModel> recipes,
            List<RecipeModel> myRecipes,
            List<RestaurantModel> restaurants,
            List<OrderModel> myOrders,
            String errorMessage,
            List<RecipeModel> orderScreenList,
            bool isMyRecipeList,
            AppLoadingStatus appLoadingStatus)?
        mainState,
  }) {
    return mainState?.call(recipes, myRecipes, restaurants, myOrders,
        errorMessage, orderScreenList, isMyRecipeList, appLoadingStatus);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initialState,
    TResult Function(
            List<RecipeModel> recipes,
            List<RecipeModel> myRecipes,
            List<RestaurantModel> restaurants,
            List<OrderModel> myOrders,
            String errorMessage,
            List<RecipeModel> orderScreenList,
            bool isMyRecipeList,
            AppLoadingStatus appLoadingStatus)?
        mainState,
    required TResult orElse(),
  }) {
    if (mainState != null) {
      return mainState(recipes, myRecipes, restaurants, myOrders, errorMessage,
          orderScreenList, isMyRecipeList, appLoadingStatus);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_RecipesInitialState value) initialState,
    required TResult Function(RecipesMainState value) mainState,
  }) {
    return mainState(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_RecipesInitialState value)? initialState,
    TResult? Function(RecipesMainState value)? mainState,
  }) {
    return mainState?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_RecipesInitialState value)? initialState,
    TResult Function(RecipesMainState value)? mainState,
    required TResult orElse(),
  }) {
    if (mainState != null) {
      return mainState(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$RecipesMainStateToJson(
      this,
    );
  }
}

abstract class RecipesMainState extends RecipesState {
  const factory RecipesMainState(
      {final List<RecipeModel> recipes,
      final List<RecipeModel> myRecipes,
      final List<RestaurantModel> restaurants,
      final List<OrderModel> myOrders,
      final String errorMessage,
      final List<RecipeModel> orderScreenList,
      final bool isMyRecipeList,
      final AppLoadingStatus appLoadingStatus}) = _$RecipesMainState;
  const RecipesMainState._() : super._();

  factory RecipesMainState.fromJson(Map<String, dynamic> json) =
      _$RecipesMainState.fromJson;

//the market recipe
  List<RecipeModel> get recipes; // the user recipes
  List<RecipeModel> get myRecipes; //all restaurants list
  List<RestaurantModel> get restaurants; //list of all orders
  List<OrderModel> get myOrders;
  String
      get errorMessage; //for thr orders screen to choose witch orders will view
  List<RecipeModel> get orderScreenList;
  bool get isMyRecipeList; // if the app is loading data or not
  AppLoadingStatus get appLoadingStatus;
  @JsonKey(ignore: true)
  _$$RecipesMainStateCopyWith<_$RecipesMainState> get copyWith =>
      throw _privateConstructorUsedError;
}
