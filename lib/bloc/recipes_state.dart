part of 'recipes_bloc.dart';

enum AppLoadingStatus { loading, done }

@freezed
sealed class RecipesState with _$RecipesState {
  const RecipesState._();
  const factory RecipesState.initialState() = _RecipesInitialState;
  const factory RecipesState.mainState({
    //the market recipe
    @Default([]) List<RecipeModel> recipes,
    // the user recipes
    @Default([]) List<RecipeModel> myRecipes,
    //all restaurants list
    @Default([]) List<RestaurantModel> restaurants,
    //list of all orders
    @Default([]) List<OrderModel> myOrders,
    @Default('') String errorMessage,
    //for thr orders screen to choose witch orders will view
    @Default([]) List<RecipeModel> orderScreenList,
    @Default(false) bool isMyRecipeList,
    // if the app is loading data or not
    @Default(AppLoadingStatus.loading) AppLoadingStatus appLoadingStatus,
  }) = RecipesMainState;
  factory RecipesState.fromJson(Map<String, dynamic> json) =>
      _$RecipesStateFromJson(json);
}
