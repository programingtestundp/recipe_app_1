import 'package:dio/dio.dart';
import 'package:recipes_app_1/models/orders/order_model.dart';
import 'package:recipes_app_1/models/restaurant/restaurant_model.dart';
import 'package:retrofit/http.dart';

import '../../models/recipes/recipes_model.dart';
import '../config/client.dart';

part 'api_services.g.dart';

@RestApi()
abstract class APIServices {
  static APIServices create() {
    Dio dio = client();

    return _APIServices(dio);
  }

//......... recipes ..........
  @GET('recipes')
  Future<List<RecipeModel>> getRecipes();

  @GET('recipes/id')
  Future<RecipeModel> getRecipe(@Path('id') String id);

  @POST('recipes')
  Future<RecipeModel> createRecipe(@Body() RecipeModel recipe);

  @DELETE('recipes/{id}')
  Future deleteRecipe(@Path('id') String id);

  @PUT('recipes/{id}')
  Future<RecipeModel> updateRecipe(
      @Path('id') String id, @Body() RecipeModel recipe);

//......... my_recipes ..........
  @GET('my_recipes')
  Future<List<RecipeModel>> getMyRecipes();

  @GET('my_recipes/id')
  Future<RecipeModel> getMyRecipe(@Path('id') String id);

  @POST('my_recipes')
  Future<RecipeModel> createMyRecipe(@Body() RecipeModel recipe);

  @DELETE('my_recipes/{id}')
  Future deleteMyRecipe(@Path('id') String id);

  @PUT('my_recipes/{id}')
  Future<RecipeModel> updateMyRecipe(
      @Path('id') String id, @Body() RecipeModel recipe);
//......... Orders ..........
  @GET('orders')
  Future<List<OrderModel>> getMyOrders();

  @GET('orders/id')
  Future<OrderModel> getMyOrder(@Path('id') String id);

  @POST('orders')
  Future<OrderModel> createOrder(@Body() OrderModel order);

  @DELETE('orders/{id}')
  Future deleteOrder(@Path('id') String id);

  @PUT('orders/{id}')
  Future<OrderModel> updateOrder(
      @Path('id') String id, @Body() OrderModel order);

//......... restaurants ..........
  @GET('restaurants')
  Future<List<RestaurantModel>> getRestaurants();

  @GET('restaurants/id')
  Future<List<RestaurantModel>> getRestaurant(@Path('id') String id);
}
