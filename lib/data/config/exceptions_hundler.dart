import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

class HandleException {
  static Future<
      ({
        Failure<dynamic, Exception>? failure,
        Success<dynamic, Exception>? success
      })> handle(Function request) async {
    Failure<dynamic, Exception>? failure;
    Success<dynamic, Exception>? success;

    await (request).call().then((response) {
      if (response != null) {
        success = Success(response);
      } else {
        failure = Failure(unknownErrorMessage: "Null Response $response");
      }
      return (success: success, failure: failure);
    }).catchError((error) {
      if (error is Exception) {
        failure = Failure(exception: error);
      } else {
        failure = Failure(unknownErrorMessage: "Error: $error");
      }
      return (success: success, failure: failure);
    });

    return (success: success, failure: failure);
  }
}

sealed class Result<S, E extends Exception> {
  const Result();
}

final class Success<S, E extends Exception> extends Result<S, E> {
  Success(this.response) {
    message = 'Success';
  }
  final S response;
  late String message;
}

final class Failure<S, E extends Exception> extends Result<S, E> {
  Failure({this.exception, this.unknownErrorMessage, this.errorDetails = ''}) {
    if (exception != null) {
      if (exception is DioException) {
        final exception = this.exception as DioException;
        message = exception.type.name;
        errorDetails = exception.response?.data['message'] ?? exception.message;
        exceptionType = 'DioException';
      } else {
        message = "Unknown Error: No Response";
        exceptionType = 'Other type of Exception';
      }
    } else if (unknownErrorMessage != null) {
      message = unknownErrorMessage ?? 'Unknown';
      exceptionType = 'Null response';
    }
    debugPrint(
      'Exception Type: $exceptionType, Message: $message, Details: $errorDetails',
    );
  }

  final E? exception;
  late String message;
  String errorDetails;
  late String? unknownErrorMessage;
  late String exceptionType;
}
