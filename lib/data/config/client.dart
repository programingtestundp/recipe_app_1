import 'package:dio/dio.dart';

Dio client() {
  Dio dio = Dio(BaseOptions(
    contentType: "application/json",
    // receiveTimeout: Duration(seconds: 15),sendTimeout: ,
    baseUrl:
        'https://api.backendless.com/FC3385C2-A5AE-1665-FFE0-FEA2F56CE000/72CC029C-AE6B-4CFE-B224-D76C960F29A3/data/',
    connectTimeout: const Duration(seconds: 10),
  ));

  return dio;
}
