import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../bloc/recipes_bloc.dart';
import '../widgets/recipe_card_widget.dart';

@RoutePage()
class RecipesScreen extends StatelessWidget {
  const RecipesScreen({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    var state = context.watch<RecipesBloc>().state;
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: state.when(
        initialState: () => const Center(child: CircularProgressIndicator()),
        mainState: (recipes, myRecipes, restaurants, myOrders, errorMessage,
            orderScreenList, isMyRecipeList, appLoadingStatus) {
          return Scaffold(
            backgroundColor: Colors.blueGrey,
            appBar: AppBar(
              title: const Center(
                child: Text(
                  'All Recipes',
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ),
            body: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              child: CustomScrollView(
                slivers: [
                  const SliverPadding(
                      padding: EdgeInsets.symmetric(vertical: 8)),
                  SliverGrid.builder(
                    itemCount: recipes.length,
                    gridDelegate:
                        const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      mainAxisSpacing: 10.0,
                      crossAxisSpacing: 10.0,
                    ),
                    itemBuilder: (BuildContext context, int index) {
                      final recipe = recipes[index];
                      final restaurant = restaurants
                          .where((restaurant) =>
                              restaurant.objectId == recipe.restaurant_id)
                          .first;
                      return RecipeCardWidget(
                        recipe: recipe,
                        restaurant: restaurant,
                        height: 167,
                      );
                    },
                  ),
                  const SliverPadding(
                      padding: EdgeInsets.symmetric(vertical: 8)),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
