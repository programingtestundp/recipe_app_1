import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:recipes_app_1/bloc/recipes_bloc.dart';
import 'package:recipes_app_1/models/recipes/recipes_model.dart';
import 'package:recipes_app_1/widgets/recipe_card_widget.dart';
import 'package:recipes_app_1/widgets/snake_bar_message_widget.dart';

import '../models/restaurant/restaurant_model.dart';
import '../widgets/info_screens_header.dart';

@RoutePage()
class RestaurantInfoScreen extends StatelessWidget {
  const RestaurantInfoScreen(
      {super.key, required this.restaurant, required this.restaurantRecipes});
  final RestaurantModel restaurant;
  final List<RecipeModel> restaurantRecipes;
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: CustomScrollView(slivers: [
          InfoScreensHeader(
              image_link: restaurant.image_link, bottomWidget: _bottomWidget()),
          ..._body(context)
        ]),
      ),
    );
  }

//restaurant header info
  Widget _bottomWidget() {
    return Text.rich(TextSpan(
        style: TextStyle(
          color: Colors.white.withOpacity(0.8),
          shadows: [
            Shadow(
              color: Colors.black.withOpacity(0.7),
              blurRadius: 4,
              offset: Offset(2, 2),
            ),
          ],
        ),
        children: [
          TextSpan(
            text: '${restaurant.name}\n',
            style: TextStyle(
                color: Colors.white.withOpacity(0.8),
                shadows: [
                  Shadow(
                    color: Colors.black.withOpacity(0.7),
                    blurRadius: 4,
                    offset: Offset(2, 2),
                  ),
                ],
                fontSize: 30),
          ),
          TextSpan(text: 'Location: ${restaurant.location},    Evaluation:'),
          for (var i = 0; i < restaurant.stars; i++)
            const TextSpan(text: ' ★ '),
        ]));
  }

// the body where we can find all recipes for the restaurant
  List<Widget> _body(BuildContext context) => [
        const SliverPadding(padding: EdgeInsets.symmetric(vertical: 8)),
        SliverToBoxAdapter(
            child: Padding(
          padding: EdgeInsets.only(left: 8.0),
          child: Row(
            children: [
              const Text(
                'All Restaurant Recipes:',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              const SizedBox(
                width: 10,
              ),
              ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      fixedSize: Size(170, 30),
                      minimumSize: Size(10, 10),
                      backgroundColor: restaurant.accept_external_order
                          ? Colors.lightBlue
                          : Colors.grey[50]),
                  onPressed: () {
                    if (restaurant.accept_external_order) {
                      _orderRecipeDialog(context);
                    } else {
                      context.showSnackBar(
                          message:
                              'The restaurant dose not accept privets order',
                          backgroundColor: Colors.black,
                          textColor: Colors.white);
                    }
                  },
                  child: Text(
                    'Order your own recipe',
                    style: TextStyle(
                        fontSize: 12,
                        color: restaurant.accept_external_order
                            ? Colors.white
                            : Colors.grey),
                  ))
            ],
          ),
        )),
        const SliverPadding(padding: EdgeInsets.symmetric(vertical: 8)),
        SliverGrid.builder(
          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            mainAxisSpacing: 10.0,
            crossAxisSpacing: 10.0,
          ),
          itemCount: 20,
          itemBuilder: (context, index) {
            return RecipeCardWidget(
                recipe: restaurantRecipes[0], restaurant: restaurant);
          },
        ),
        const SliverPadding(padding: EdgeInsets.symmetric(vertical: 20)),
      ];
// input the user recipe data
  _orderRecipeDialog(BuildContext context) {
    TextEditingController nameController = TextEditingController();
    TextEditingController ingredientsController = TextEditingController();
    TextEditingController descriptionController = TextEditingController();
    return showDialog(
      context: context,
      builder: (context) {
        return Dialog(
          backgroundColor: Colors.grey[50],
          child: Container(
            width: 200,
            height: 400,
            padding: const EdgeInsets.all(10),
            child: SingleChildScrollView(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    TextField(
                      controller: nameController,
                      maxLines: 1,
                      decoration: InputDecoration(
                        hintText: 'Name of your recipe',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                    ),
                    TextField(
                      controller: ingredientsController,
                      maxLines: 3,
                      decoration: InputDecoration(
                        hintText:
                            'Ingredients for your recipe like this:\ntomato,potato,onion...',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                    ),
                    TextField(
                      controller: descriptionController,
                      maxLines: 3,
                      decoration: InputDecoration(
                        hintText: 'Description',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                    ),
                    ElevatedButton(
                        onPressed: () {
                          final condition = nameController.text != '' &&
                              ingredientsController.text != '' &&
                              descriptionController.text != '';
                          if (condition) {
                            final ingredients =
                                ingredientsController.text.split(',').toList();
                            final recipe = RecipeModel(
                                name: nameController.text,
                                image_link: '',
                                ingredients: ingredients,
                                description: descriptionController.text,
                                category: '',
                                price: 0,
                                restaurant_id: restaurant.objectId);
                            context.read<RecipesBloc>().add(
                                RecipesEvent.orderMyRecipe(recipe: recipe));
                            Navigator.of(context).pop(true);
                          } else {
                            context.showSnackBar(message: 'Empty Fields');
                          }
                        },
                        child: const Text('Order'))
                  ]),
            ),
          ),
        );
      },
    );
  }
}
