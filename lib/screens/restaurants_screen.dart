// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:recipes_app_1/models/recipes/recipes_model.dart';
import 'package:recipes_app_1/models/restaurant/restaurant_model.dart';
import 'package:recipes_app_1/utils/app_router.gr.dart';

import '../bloc/recipes_bloc.dart';

@RoutePage()
class RestaurantsScreen extends StatelessWidget {
  const RestaurantsScreen({
    Key? key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    var state = context.watch<RecipesBloc>().state;
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: state.when(
        initialState: () => const Center(child: CircularProgressIndicator()),
        mainState: (recipes, myRecipes, restaurants, myOrders, errorMessage,
            orderScreenList, isMyRecipeList, appLoadingStatus) {
          return Scaffold(
            backgroundColor: Colors.white,
            appBar: AppBar(
              title: const Center(
                child: Text(
                  'All Restaurants',
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ),
            body: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8),
                child: ListView.builder(
                  itemCount: restaurants.length,
                  itemBuilder: (context, index) {
                    final restaurant = restaurants[index];
                    return _restaurantCard(context, restaurant, recipes);
                  },
                )),
          );
        },
      ),
    );
  }

  Widget _restaurantCard(BuildContext context, RestaurantModel restaurant,
      List<RecipeModel> recipes) {
    return InkWell(
      onTap: () {
        final restaurantRecipes = recipes
            .where((recipe) => recipe.restaurant_id == restaurant.objectId)
            .toList();
        context.router.push(
          RestaurantInfoRoute(
              restaurant: restaurant, restaurantRecipes: restaurantRecipes),
        );
      },
      child: Container(
        margin: const EdgeInsets.symmetric(vertical: 8),
        width: double.maxFinite,
        height: 160,
        decoration: BoxDecoration(
          color: Colors.grey.withOpacity(0.6),
          borderRadius: BorderRadius.circular(8),
          image: DecorationImage(
              image: NetworkImage(restaurant.image_link), fit: BoxFit.cover),
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.2),
              blurRadius: 4,
              offset: const Offset(0, 2),
            ),
          ],
        ),
        alignment: Alignment.bottomLeft,
        padding: EdgeInsets.all(8),
        child: RichText(
            text: TextSpan(
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 15,
                  shadows: [
                    Shadow(
                      color: Colors.black.withOpacity(0.7),
                      blurRadius: 4,
                      offset: Offset(2, 2),
                    ),
                  ],
                ),
                children: [
              TextSpan(
                  text: "${restaurant.name}\n",
                  style: TextStyle(
                    fontSize: 20,
                    decoration: TextDecoration.underline,
                    decorationColor: Colors.white.withOpacity(0.6),
                    decorationThickness: 2,
                    fontWeight: FontWeight.bold,
                  )),
              TextSpan(text: "Location: ${restaurant.location}\n"),
              for (var i = 0; i < restaurant.stars; i++)
                const TextSpan(text: ' ★ '),
            ])),
      ),
    );
  }
}
