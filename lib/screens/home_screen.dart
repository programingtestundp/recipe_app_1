import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:recipes_app_1/utils/app_router.gr.dart';
import 'package:recipes_app_1/widgets/snake_bar_message_widget.dart';
import '../bloc/recipes_bloc.dart';

@RoutePage()
class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocListener<RecipesBloc, RecipesState>(
      listener: (context, state) {
        // for showing the error message
        final mainState = state as RecipesMainState;
        if (mainState.errorMessage != '') {
          context.showSnackBar(message: mainState.errorMessage);
        }
      },
      child: AutoTabsRouter.tabBar(
          routes: const [RecipesRoute(), RestaurantsRoute(), OrdersRoute()],
          builder: (context, child, controller) {
            final tabsRouter = AutoTabsRouter.of(context);
            return Scaffold(
              body: child,
              bottomNavigationBar: BottomNavigationBar(
                backgroundColor: Color.fromARGB(255, 218, 234, 246),
                currentIndex: tabsRouter.activeIndex,
                onTap: tabsRouter.setActiveIndex,
                items: const [
                  BottomNavigationBarItem(
                      icon: Icon(Icons.receipt_long_outlined),
                      label: 'Recipes'),
                  BottomNavigationBarItem(
                      icon: Icon(Icons.restaurant_menu_outlined),
                      label: 'Restaurants'),
                  BottomNavigationBarItem(
                      icon: Icon(Icons.menu_book), label: 'Orders'),
                ],
              ),
            );
          }),
    );
  }
}
