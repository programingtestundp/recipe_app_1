import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:recipes_app_1/bloc/recipes_bloc.dart';
import 'package:recipes_app_1/models/recipes/recipes_model.dart';
import 'package:recipes_app_1/models/restaurant/restaurant_model.dart';
import '../utils/app_router.gr.dart';
import '../widgets/info_screens_header.dart';

//review all the details and user options for a recipe
@RoutePage()
class RecipesInfoScreen extends StatelessWidget {
  const RecipesInfoScreen(
      {super.key, required this.recipe, required this.restaurant});

  final RecipeModel recipe;
  final RestaurantModel restaurant;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(slivers: [
        InfoScreensHeader(
          image_link: recipe.image_link,
          bottomWidget: _bottomWidget(),
        ),
        ..._body(context, recipe.ingredients)
      ]),
    );
  }

  List<Widget> _body(BuildContext context, List<String> ingredients) {
    return [
      const SliverPadding(padding: EdgeInsets.symmetric(vertical: 8)),

      //restaurant information & recipe description
      SliverToBoxAdapter(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8),
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
            Text(
              recipe.description,
              textAlign: TextAlign.center,
            ),
            Divider(),
            Row(
              children: [
                //restaurant information
                Expanded(
                  flex: 6,
                  child: GestureDetector(
                    onTap: () {
                      _navigateToRestaurantInfo(context);
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        CircleAvatar(
                          radius: 25,
                          backgroundImage: NetworkImage(restaurant.image_link),
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                        RichText(
                            text: TextSpan(
                                text: '${restaurant.name}\n',
                                style: const TextStyle(color: Colors.black),
                                children: [
                              for (var i = 0; i < restaurant.stars; i++)
                                const TextSpan(text: ' ★ '),
                            ])),
                      ],
                    ),
                  ),
                ),
                //order bottom
                Expanded(
                    flex: 4,
                    child: BlocBuilder<RecipesBloc, RecipesState>(
                      builder: (context, state) {
                        final mainState = state as RecipesMainState;
                        //check if the recipe is requested or not
                        final isRequested = mainState.myOrders.any((order) =>
                            mainState.recipes.any((recipe) =>
                                recipe.objectId == order.recipe_id));

                        return ElevatedButton(
                            style: ElevatedButton.styleFrom(
                                backgroundColor:
                                    isRequested ? Colors.redAccent : null),
                            onPressed: () {
                              if (isRequested) {
                                final order = state.myOrders
                                    .where((order) =>
                                        order.recipe_id == recipe.objectId)
                                    .first;
                                context
                                    .read<RecipesBloc>()
                                    .add(RecipesEvent.deleteOrder(order));
                              } else {
                                context.read<RecipesBloc>().add(
                                    RecipesEvent.orderRecipe(recipe: recipe));
                              }
                            },
                            child: isRequested
                                ? const Text('Ordered',
                                    style: TextStyle(color: Colors.white))
                                : const Text('Order'));
                      },
                    ))
              ],
            ),
            Divider(),
          ]),
        ),
      ),
      const SliverPadding(padding: EdgeInsets.symmetric(vertical: 8)),
      SliverGrid.builder(
        itemCount: ingredients.length,
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            mainAxisSpacing: 10.0,
            crossAxisSpacing: 10.0,
            mainAxisExtent: 40),
        itemBuilder: (BuildContext context, int index) {
          final ingredient = ingredients[index];

          return Card(
            elevation: 3,
            color: Color.fromARGB(255, 113, 172, 198),
            child: Center(child: Text(ingredient)),
            shape:
                BeveledRectangleBorder(borderRadius: BorderRadius.circular(5)),
          );
        },
      ),
    ];
  }

//the information for the sliver app bar
  Widget _bottomWidget() {
    return Column(
      children: [
        Text(
          recipe.name,
          style: const TextStyle(
              fontWeight: FontWeight.bold, color: Colors.white, fontSize: 25),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
                flex: 1,
                child: Center(
                    child: Text(
                  'category: ${recipe.category}',
                  style: const TextStyle(
                    color: Colors.white,
                  ),
                ))),
            Expanded(
              flex: 1,
              child: Center(
                child: Text(
                  'Price: ${recipe.price}\$',
                  style: const TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }

// ............... Functions ................
  void _navigateToRestaurantInfo(BuildContext context) {
    //get recipes
    final recipes = context.read<RecipesBloc>().state.maybeWhen(
          orElse: () => <RecipeModel>[],
          mainState: (recipes, myRecipes, restaurants, myOrders, errorMessage,
                  orderScreenList, isMyRecipeList, appLoadingStatus) =>
              recipes,
        );
    //get restaurant recipes
    final restaurantRecipes = recipes
        .where((recipe) => recipe.restaurant_id == restaurant.objectId)
        .toList();
    //navigate to restaurant information page
    context.router.push(
      RestaurantInfoRoute(
          restaurant: restaurant, restaurantRecipes: restaurantRecipes),
    );
  }
}
