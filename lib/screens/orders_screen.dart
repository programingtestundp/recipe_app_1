import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:recipes_app_1/bloc/recipes_bloc.dart';
import 'package:recipes_app_1/models/orders/order_model.dart';
import 'package:recipes_app_1/models/recipes/recipes_model.dart';
import 'package:recipes_app_1/widgets/snake_bar_message_widget.dart';

@RoutePage()
class OrdersScreen extends StatelessWidget {
  const OrdersScreen({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    final state = context.watch<RecipesBloc>().state;
    return Scaffold(
      appBar: AppBar(
        title: const Center(
            child: Text(
          'Orders',
          style: TextStyle(color: Colors.white),
        )),
      ),
      body: Builder(builder: (context) {
        return state.when(
          initialState: () => SizedBox(),
          mainState: (recipes, myRecipes, restaurants, myOrders, errorMessage,
              orderScreenList, isMyRecipeList, appLoadingStatus) {
            return Column(
              children: [
                _changeListButton(context, isMyRecipeList),
                Expanded(
                    child: _orderScreenList(
                        isMyRecipeList, myRecipes, orderScreenList, myOrders))
              ],
            );
          },
        );
      }),
    );
  }

  ElevatedButton _changeListButton(BuildContext context, bool isMyRecipeList) {
    return ElevatedButton(
        onPressed: () {
          //change the list for ist view
          context.read<RecipesBloc>().add(const RecipesEvent.fillOrdersList());
        },
        style: ElevatedButton.styleFrom(
            backgroundColor: isMyRecipeList
                ? const Color.fromARGB(255, 161, 23, 119)
                : Colors.blue),
        child: isMyRecipeList
            ? const Text(
                'My Orders',
                style: TextStyle(color: Colors.white),
              )
            : const Text(
                'Orders',
                style: TextStyle(color: Colors.white),
              ));
  }

  ListView _orderScreenList(bool isMyRecipeList, List<RecipeModel> myRecipes,
      List<RecipeModel> orderScreenList, List<OrderModel> myOrders) {
    return ListView.builder(
      itemCount: isMyRecipeList ? myRecipes.length : orderScreenList.length,
      itemBuilder: (context, index) {
        final recipe =
            isMyRecipeList ? myRecipes[index] : orderScreenList[index];
        return Padding(
          padding: const EdgeInsets.all(10.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(recipe.name),

              //delete order
              TextButton(
                  onPressed: () {
                    final orders = myOrders
                        .where((order) => order.recipe_id == recipe.objectId);

                    if (orders.isNotEmpty) {
                      context
                          .read<RecipesBloc>()
                          .add(RecipesEvent.deleteOrder(orders.first));
                    } else {
                      context.showSnackBar(message: 'Order Not Found !');
                    }
                  },
                  child: const Text(
                    'Delete',
                    style: TextStyle(color: Colors.red),
                  ))
            ],
          ),
        );
      },
    );
  }
}
