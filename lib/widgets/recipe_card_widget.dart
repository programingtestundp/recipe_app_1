import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:recipes_app_1/models/recipes/recipes_model.dart';
import 'package:recipes_app_1/models/restaurant/restaurant_model.dart';
import 'package:recipes_app_1/utils/app_router.gr.dart';

class RecipeCardWidget extends StatelessWidget {
  const RecipeCardWidget(
      {super.key,
      this.width = double.maxFinite,
      this.height = double.maxFinite,
      this.radius = 25,
      required this.recipe,
      required this.restaurant});
  final double width;
  final double height;
  final double radius;
  final RecipeModel recipe;
  final RestaurantModel restaurant;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        context.router.push(
          RecipesInfoRoute(recipe: recipe, restaurant: restaurant),
        );
      },
      child: Container(
        width: width,
        height: height,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(radius),
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.2),
              blurRadius: 4,
              offset: const Offset(0, 2),
            ),
          ],
        ),
        padding: const EdgeInsets.all(8),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(
                flex: 20,
                child: SizedBox(
                  width: width * .9,
                  child: DecoratedBox(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.vertical(
                              top: Radius.circular(radius)),
                          image: DecorationImage(
                              image: NetworkImage(recipe.image_link),
                              fit: BoxFit.fill))),
                ),
              ),
              Spacer(
                flex: 2,
              ),
              Expanded(
                flex: 4,
                child: Align(
                  alignment: Alignment.center,
                  child: Text(
                    recipe.name,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: const TextStyle(
                        color: Colors.black, fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              const Expanded(
                  child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 8),
                child: Divider(
                  color: Colors.black54,
                ),
              )),
              Expanded(
                flex: 5,
                child: Text(
                  '${restaurant.name}\n',
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: const TextStyle(color: Colors.black),
                ),
              ),
              Expanded(
                flex: 5,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    RichText(
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        text: TextSpan(
                            style: const TextStyle(color: Colors.black),
                            children: [
                              for (var i = 0; i < restaurant.stars; i++)
                                const TextSpan(text: '★'),
                            ])),
                    Text(
                      '${recipe.price}\$ ',
                      style: const TextStyle(fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              )
            ]),
      ),
    );
  }
}
