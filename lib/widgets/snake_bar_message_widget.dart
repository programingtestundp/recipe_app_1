import 'package:flutter/material.dart';

extension ShowSnackBar on BuildContext {
  void showSnackBar(
      {required String message,
      Color backgroundColor = Colors.black,
      Color textColor = Colors.white,
      int duration = 5000}) {
    ScaffoldMessenger.of(this).showSnackBar(
      SnackBar(
        duration: Duration(milliseconds: duration),
        content: Center(
          child: Text(
            message,
            style: TextStyle(color: textColor),
          ),
        ),
        backgroundColor: backgroundColor,
      ),
    );
  }
}
