import 'package:flutter/material.dart';

class InfoScreensHeader extends StatelessWidget {
  const InfoScreensHeader(
      {super.key, required this.image_link, required this.bottomWidget});
  final String image_link;
  final Widget bottomWidget;
  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      collapsedHeight: 70,
      pinned: true,
      stretch: true,
      backgroundColor: Colors.transparent,
      expandedHeight: 200.0,
      elevation: 1,
      automaticallyImplyLeading: false,
      flexibleSpace: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              fit: BoxFit.cover,
              colorFilter: ColorFilter.mode(
                  Colors.black.withOpacity(0.5), BlendMode.srcATop),
              image: NetworkImage(image_link)),
        ),
        alignment: Alignment.center,
      ),
      bottom: PreferredSize(
        preferredSize: Size(double.maxFinite, 10),
        child: Container(
            alignment: Alignment.centerLeft,
            padding: const EdgeInsets.only(bottom: 8),
            child: bottomWidget),
      ),
    );
  }
}
